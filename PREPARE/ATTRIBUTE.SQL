SELECT --top 100
Code
,[Webshop Description] as WSDesc
,isnull((select [Description] from %1.dbo.[%2$Extension Translation] WHERE [Table ID] = 10000784 and [Field ID] = 50001 and Code = A.Code and [Language Code] = 'FRS'),'') as WSDescFRS
,[Webshop Filter Type Code]
--,[Unit of Measure Code] as UOMCode
,isnull((select [Description] from %1.dbo.[%2$Unit of Measure] WHERE Code = [Unit of Measure Code]),'') as UOMDesc
,isnull((select [Description] from %1.dbo.[%2$Unit of Measure Translation] WHERE Code = [Unit of Measure Code] AND [Language Code] = 'FRS'),'') as UOMDescFRS
FROM %1.dbo.[%2$Attribute] as A

