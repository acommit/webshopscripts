SELECT
  isnull(V.[Common Item No_],'') AS CommonItemNo,   
  I.[Description] as ItemDescription,
  isnull(VD1.[Value Description],'') AS VarDim1,  
  isnull(VD2.[Value Description],'') AS VarDim2,
  I.[Item Family Code] as ItemFamilyCode,
  isnull((select substring([Attribute Value],CHARINDEX('#',[Attribute Value]) + 1,10) from %1.dbo.[%2$Attribute Value] (NOLOCK) where I.[No_] = [Link Field 1] and [Attribute Code] = 'ALLG_SHOPLABEL'),'') as ALLG_SHOPLABEL,
  isnull(IVS.[Status Code],'') as ItemStatusCode,
  isnull((select substring([Attribute Value],CHARINDEX('#',[Attribute Value]) + 1,10) from %1.dbo.[%2$Attribute Value] (NOLOCK) where I.[No_] = [Link Field 1] and [Attribute Code] = 'ALLG_GENDER'),'') as ALLG_GENDER,
  isnull((select substring([Attribute Value],CHARINDEX('#',[Attribute Value]) + 1,10) from %1.dbo.[%2$Attribute Value] (NOLOCK) where I.[No_] = [Link Field 1] and [Attribute Code] = 'ALLG_VELOCARD'),'') as ALLG_VELOCARD,
  isnull(SP.[Promotion Type],isnull(SP_Item.[Promotion Type],0)) as ItemPromotionType,
  isnull(V.[Allocation Key],'') AS VarAllocationKey,
  isnull((select substring([Attribute Value],1,1) from %1.dbo.[%2$Attribute Value] (NOLOCK) where I.[No_] = [Link Field 1] and [Attribute Code] = 'DIAS_LONGTAIL'),'') as DIAS_LONGTAIL,
  isnull((select substring([Attribute Value],CHARINDEX('#',[Attribute Value]) + 1,10) from %1.dbo.[%2$Attribute Value] (NOLOCK) where I.[No_] = [Link Field 1] and [Attribute Code] = 'ALLG_TEILSHOP'),'') as ALLG_TEILSHOP,
  isnull((select TOP 1 substring([Attribute Value],CHARINDEX('#',[Attribute Value]) + 1,10) from %1.dbo.[%2$Attribute Value] (NOLOCK) where I.[No_] = [Link Field 1] and [Attribute Code] = 'ALLG_VELOVERSAND'),'') as ALLG_VELOVERSAND,
  isnull((select substring([Attribute Value],CHARINDEX('#',[Attribute Value]) + 1,10) from %1.dbo.[%2$Attribute Value] (NOLOCK) where I.[No_] = [Link Field 1] and [Attribute Code] = 'ALLG_WARENKORB'),'') as ALLG_WARENKORB,
  --isnull((select substring([Attribute Value],CHARINDEX('#',[Attribute Value]) + 1,10) from %1.dbo.[%2$Attribute Value] (NOLOCK) where I.[No_] = [Link Field 1] and [Attribute Code] = 'ALLG_SHOPSPERRE'),'') as ALLG_SHOPSPERRE,
  isnull((select [Name] from %1.dbo.[Country_Region] where Code = I.[Country_Region of Origin Code]),'') as ItemCountryOfOrigin,
  isnull(I.[No_]+ '.'+ V.[Code],'') AS ItemNoVarianteCode,  
  I.[Gen_ Prod_ Posting Group] as ItemGenProdPostingGroup,
  I.[Base Unit of Measure] as ItemBaseUOM,
  I.[Product Group Code] as ItemProdGroupCode,
  I.[VAT Prod_ Posting Group] as ItemVATProdPostingGroup,
  isnull((select substring([Attribute Value],CHARINDEX('#',[Attribute Value]) + 1,10) from %1.dbo.[%2$Attribute Value] (NOLOCK) where I.[No_] = [Link Field 1] and [Attribute Code] = 'ALLG_OBJEKT_TYP'),'') as ALLG_OBJEKT_TYP
  ,isnull(SP.[Unit Price Including VAT],isnull(SP_Item.[Unit Price Including VAT],0)) as VarPriceInclVAT
  ,isnull(SP.[Last Unit Price Before Promo],isnull(SP_Item.[Last Unit Price Before Promo],0)) as VarPriceInclVATB4Promo
  ,CAST(NULL as datetime) as ItemDateCreated
  ,CAST(NULL as datetime) as ItemLastDateModified
  ,I.[Lead Time Calculation] as ItemLeadTimeCalculation
  ,isnull(PP.[Direct Unit Cost],isnull(PP_Item.[Direct Unit Cost],0)) as VarDirectUnitCost
  ,isnull(PP.[Currency Code],isnull(PP_Item.[Currency Code],'')) as CurrencyCode
  ,isnull(PP.[Packaging Unit],isnull(PP_Item.[Packaging Unit],0)) as PackagingUnit
  ,isnull(PP.[Conversion Purchase-Inventory],isnull(PP_Item.[Conversion Purchase-Inventory],0)) as ConversionPurchInvt  
  ,I.[Zero Price Valid]
  ,I.[Vendor No_] 
  ,I.[Purchaser Code] as PurchaserCode
  ,I.[Purch_ Unit of Measure] as PurchUOM
  ,isnull((select [Qty_ per Unit of Measure] from %1.dbo.[%2$Item Unit of Measure] as IUM where IUM.[Item No_] = I.No_ AND Code = I.[Purch_ Unit of Measure]),0) as QtyPerPurchUOM
  ,V.Description as VarDescription
  ,V.[Description 2] as VarDescription2
  ,V.[Webshop excluded] as WSExcl
  ,isnull(nullif(IVR.[Season Code], ''), I.[Season Code]) AS VarSeasonCode 
  ,isnull(IVT.[Text],'') as ItemHTMLText
  ,isnull(IVT2.[Text],'') as ItemHTMLTextPurch
  ,isnull(IV.[Vendor Item No_],'') as VendorItemNo
  ,isnull(IV.[Barcode No_],'') as VendorBarcodeNo
  ,isnull((select [Description] from %1.dbo.[%2$Item Family] where Code = I.[Item Family Code]),'') as ItemFamilyDesc
  ,isnull(BC.[Preorder Discount _],0) as BrandDiscount1
  ,isnull(BC.[Reassort Discount _],0) as BrandDiscount2
  ,isnull(BC.[Special Order Discount _],0) as BrandDiscount3
  ,isnull(BC.[Employes Discount _],0) as BrandDiscount4
  ,V.[Location Setting]
  ,V.[Lagerbereich]
  ,V.[Mindestbestand]
  ,V.[Losgr�sse]
  ,V.[Ganze Losgr�sse]
  ,V.[Berechnung mittlerer EP]
  ,isnull((select top 1 [Numeric Value] from %1.dbo.[%2$Attribute Value] (NOLOCK) where I.[No_] = [Link Field 1] and V.[Code] = [Link Field 2] and [Attribute Code] = 'ALLG_GEWICHT_G'),0) as [ALLG_GEWICHT_G]
  FROM  %1.dbo.[%2$Item] AS I (NOLOCK)
  INNER JOIN %1.dbo.[%2$Item Variant] AS V (NOLOCK) ON I.[No_] = V.[Item No_]
  LEFT JOIN %1.dbo.[%2$Item Variant Text] AS IVT (NOLOCK) ON IVT.[ItemNo] = I.[No_] and IVT.VarCode in ('',V.Code) and EXISTS (Select * FROM %1.dbo.[%2$Extended Text Header] ETH (NOLOCK) where ETH.[Text No_] = IVT.TextNo and ETH.No_ = IVT.[ItemNo] and ETH.Description = 'Marketingtext')  
  LEFT JOIN %1.dbo.[%2$Item Variant Text] AS IVT2 (NOLOCK)  ON IVT2.[ItemNo] = I.[No_] and IVT2.VarCode in ('',V.Code) and EXISTS (Select * FROM %1.dbo.[%2$Extended Text Header] ETH (NOLOCK) where ETH.[Text No_] = IVT2.TextNo and ETH.No_ = IVT2.[ItemNo] and ETH.Description = 'Einkauf')
  
  LEFT JOIN %1.dbo.[%2$Item Variant Registration] AS IVR (NOLOCK) ON I.[No_] = IVR.[Item No_] and IVR.Variant = V.Code 
  OUTER APPLY (select TOP 1 [Status Code] from %1.dbo.[%2$Item Status Link] (NOLOCK) where I.[No_] = [Item No_] and [Variant Code] = V.Code
               AND [Starting Date] <= GETDATE() Order by [Starting Date] Desc
			) AS IVS 
  OUTER APPLY (SELECT TOP 1 [Unit Price Including VAT], [Promotion Type], [Last Unit Price Before Promo] 
             FROM %1.dbo.[%2$Sales Price] (NOLOCK) 
			 WHERE [Item No_] = I.No_ and [Sales Code] = 'ALL' AND [Variant Code] = V.Code AND [Starting Date] <= GETDATE() AND ([Ending Date] > GETDATE()-1 Or [Ending Date] = '1753-01-01 00:00:00.000')
			 ORDER BY [Starting Date] desc) AS SP 
  OUTER APPLY (SELECT TOP 1 [Unit Price Including VAT], [Promotion Type], [Last Unit Price Before Promo] 
             FROM %1.dbo.[%2$Sales Price] (NOLOCK) 
			 WHERE [Item No_] = I.No_ and [Sales Code] = 'ALL' AND [Variant Code] = '' AND [Starting Date] <= GETDATE() AND ([Ending Date] > GETDATE()-1 Or [Ending Date] = '1753-01-01 00:00:00.000')
			 ORDER BY [Starting Date] desc) AS SP_Item
  OUTER APPLY (select [Vendor Item No_],[Barcode No_] from %1.dbo.[%2$Item Vendor] IV where [Vendor No_] = I.[Vendor No_] and IV.[Item No_] = I.No_ and IV.[Variant Code] = V.Code) AS IV
  OUTER APPLY (SELECT TOP 1 [Direct Unit Cost],[Currency Code],[Packaging Unit],[Conversion Purchase-Inventory]
             FROM %1.dbo.[%2$Purchase Price] (NOLOCK) 
			 WHERE [Item No_] = I.No_ and [Variant Code] = V.Code AND [Starting Date] <= GETDATE() AND ([Ending Date] > GETDATE()-1 Or [Ending Date] = '1753-01-01 00:00:00.000')
			 ORDER BY [Starting Date] desc) AS PP 
  OUTER APPLY (SELECT TOP 1 [Direct Unit Cost],[Currency Code],[Packaging Unit],[Conversion Purchase-Inventory]
             FROM %1.dbo.[%2$Purchase Price] (NOLOCK) 
			 WHERE [Item No_] = I.No_ and [Variant Code] = '' AND [Starting Date] <= GETDATE() AND ([Ending Date] > GETDATE()-1 Or [Ending Date] = '1753-01-01 00:00:00.000')
			 ORDER BY [Starting Date] desc) AS PP_Item
  OUTER APPLY (SELECT TOP 1 [Preorder Discount _],[Reassort Discount _],[Special Order Discount _],[Employes Discount _]
             FROM %1.dbo.[%2$Brand Condition] BC (NOLOCK) 
			 WHERE BC.[Vendor No_] = I.[Vendor No_] and BC.[Item Family Code] = I.[Item Family Code] AND [Valid from Date] <= GETDATE() AND ([Valid until Date] > GETDATE()-1 Or [Valid until Date] = '1753-01-01 00:00:00.000')
			) AS BC 
  OUTER APPLY (SELECT TOP 1 [Value Description] From %1.dbo.[%2$Extended Variant Values] (NOLOCK)
             WHERE [Item No_] = IVR.[Item No_] and [Value] = IVR.[Variant Dimension 1] and Dimension = 1) AS VD1
  OUTER APPLY (SELECT TOP 1 [Value Description] From %1.dbo.[%2$Extended Variant Values] (NOLOCK)
             WHERE [Item No_] = IVR.[Item No_] and [Value] = IVR.[Variant Dimension 2] and Dimension = 2) AS VD2
