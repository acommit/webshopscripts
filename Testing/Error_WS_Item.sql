--NAV 2017 PROD for Velo+ - powered by Acommit
---------------------------

--Fehler bei einem Aufruf von System.Data.SqlClient.SqlCommand.ExecuteNonQuery mit folgender Meldung: Cannot insert the value NULL into column 'AssortCode', table 'Webshop_PROD.dbo.WS_ItemAssortment_SET'; column does not allow nulls. UPDATE fails.
--The statement has been terminated., 
BEGIN TRANSACTION;

DECLARE @ChangeLog TABLE
(
	ItemNo varchar(50),
    ChangedOn datetime
);

--Item
MERGE [WS_Item_SET] AS [Target] 
USING (SELECT 
  I.[No_] as No,
  isnull(IT.[Description], I.[Description]) as [Description],
  isnull(IT.[Description 2], I.[Description 2]) as [Description2],
  isnull(I.[Webshop Description],'') as ShopTitle,
  I.[Sales Unit of Measure] as [SalesUnitofMeasure],  
  I.[Vendor Item No_] as [VendorItemNo],
  I.[Vendor No_] as [VendorNo],
  I.[Item Family Code] as [ItemFamilyCode],
  isnull(IFM.[Description],'') as [IFDescription],
  isnull(IT.[Description],'') as [IFTDescription],
  I.[Last Date Modified] as [LastDateModified],
  I.[Pictures updated] as [Picturesupdated]
FROM  [VELO_NAV100CHLS_PROD].dbo.[VeloPlus$Item] AS I (NOLOCK)
LEFT JOIN [VELO_NAV100CHLS_PROD].dbo.[VeloPlus$Item Family] AS IFM (NOLOCK) ON I.[Item Family Code] = IFM.[Code]
LEFT JOIN [VELO_NAV100CHLS_PROD].dbo.[VeloPlus$Item Translation] AS IT (NOLOCK) ON I.[No_] = IT.[Item No_] and IT.[Variant Code] = '' and IT.[Language Code] = ''
WHERE EXISTS (SELECT V.[Item No_] from [VELO_NAV100CHLS_PROD].dbo.[VeloPlus$Item Variant] AS V (NOLOCK) WHERE I.[No_] = V.[Item No_] and V.[Webshop excluded]=0 and V.[Webshop approved]=1)
) AS [Source]
ON (Target.[No] = Source.[No])
WHEN MATCHED AND
(
      Target.[No] <> Source.[No]
   OR Target.[Description] <> Source.[Description]
   OR Target.[Description2] <> Source.[Description2]
   OR Target.[ShopTitle] <> Source.[ShopTitle]
   OR Target.[SalesUnitofMeasure] <> Source.[SalesUnitofMeasure]
   OR Target.[VendorItemNo] <> Source.[VendorItemNo]
   OR Target.[VendorNo] <> Source.[VendorNo]
   OR Target.[ItemFamilyCode] <> Source.[ItemFamilyCode]
   OR Target.[IFDescription] <> Source.[IFDescription]
   OR Target.[IFTDescription] <> Source.[IFTDescription]
   OR Target.[LastDateModified] <> Source.[LastDateModified]
   OR Target.[Picturesupdated] <> Source.[Picturesupdated]
)
THEN UPDATE SET     
   Target.[No] = Source.[No]
  ,Target.[Description] = Source.[Description]
  ,Target.[Description2] = Source.[Description2]
  ,Target.[ShopTitle] = Source.[ShopTitle]
  ,Target.[SalesUnitofMeasure] = Source.[SalesUnitofMeasure]
  ,Target.[VendorItemNo] = Source.[VendorItemNo]
  ,Target.[VendorNo] = Source.[VendorNo]
  ,Target.[ItemFamilyCode] = Source.[ItemFamilyCode]
  ,Target.[IFDescription] = Source.[IFDescription]
  ,Target.[IFTDescription] = Source.[IFTDescription]
  ,Target.[LastDateModified] = Source.[LastDateModified]
  ,Target.[Picturesupdated] = Source.[Picturesupdated]
WHEN NOT MATCHED BY TARGET 
THEN INSERT (
   [No]
  ,[Description]
  ,[Description2]
  ,[ShopTitle]
  ,[SalesUnitofMeasure]
  ,[VendorItemNo]
  ,[VendorNo]
  ,[ItemFamilyCode]
  ,[IFDescription]
  ,[IFTDescription]
  ,[LastDateModified]
  ,[Picturesupdated]       
 )
VALUES (
	Source.[No]
   ,Source.[Description]
   ,Source.[Description2]
   ,Source.[ShopTitle]
   ,Source.[SalesUnitofMeasure]
   ,Source.[VendorItemNo]
   ,Source.[VendorNo]
   ,Source.[ItemFamilyCode]
   ,Source.[IFDescription]
   ,Source.[IFTDescription]
   ,Source.[LastDateModified]
   ,Source.[Picturesupdated]   
   )
WHEN NOT MATCHED BY SOURCE THEN DELETE
OUTPUT COALESCE(Inserted.No,Deleted.No),getdate() INTO @ChangeLog ;

INSERT INTO [dbo].[WS_Item_LOG] ([No],[Generated at],[Export State],[Exported at])
SELECT ItemNo, ChangedOn, 0, NULL
from @ChangeLog as cl
inner join [WS_Item_SET] item_set on item_set.[No] = cl.ItemNo
Where not exists(select * from [dbo].[WS_Item_LOG] ItemLog where ItemLog.[No]=cl.ItemNo and ItemLog.[Export State] = 0)
delete from @ChangeLog

--Item Assortment
MERGE [WS_ItemAssortment_SET] AS [Target] 
USING ( SELECT 
	I.[No_] as [No],
    AD.Code AS AssortCode,  
    AD.[Sorting ID] AS AssortSortingID
	FROM  [VELO_NAV100CHLS_PROD].dbo.[VeloPlus$Item] AS I (NOLOCK)
	LEFT join (SELECT I.[No_], AD.Code, AD.[Sorting ID] from [VELO_NAV100CHLS_PROD].dbo.[VeloPlus$eCommerce Assort_ Def_] AS AD (NOLOCK) 
	INNER JOIN [VELO_NAV100CHLS_PROD].dbo.[VeloPlus$Item] I on
	AD.[Condition Type]=0 and AD.[Type] = 0 and AD.[Value] = I.[No_]
	UNION
	SELECT I.[No_], AD.Code, AD.[Sorting ID] from [VELO_NAV100CHLS_PROD].dbo.[VeloPlus$eCommerce Assort_ Def_] AS AD (NOLOCK) 
	INNER JOIN [VELO_NAV100CHLS_PROD].dbo.[VeloPlus$Item] I on
	AD.[Condition Type]=0 and AD.[Type] = 1 and AD.[Value] = I.[Product Group Code]
	UNION
	SELECT I.[No_], AD.Code, AD.[Sorting ID] from [VELO_NAV100CHLS_PROD].dbo.[VeloPlus$eCommerce Assort_ Def_] AS AD (NOLOCK) 
	INNER JOIN [VELO_NAV100CHLS_PROD].dbo.[VeloPlus$Item] I on
	AD.[Condition Type]=0 and AD.[Type] = 2 and AD.[Value] = I.[Item Category Code]
	UNION
	SELECT I.[No_], AD.Code, AD2.[Sorting ID] from [VELO_NAV100CHLS_PROD].dbo.[VeloPlus$eCommerce Assort_ Def_] AS AD (NOLOCK) 
	INNER JOIN [VELO_NAV100CHLS_PROD].dbo.[VeloPlus$eCommerce Assort_ Def_] AS AD2 on
	AD2.[Condition Type]=0 and AD2.[Type]=0 and AD2.Code = AD.[Value]
	INNER JOIN [VELO_NAV100CHLS_PROD].dbo.[VeloPlus$Item] I on
	I.[No_] = AD2.[Value]
	WHERE AD.[Condition Type]=0 and AD.[Type] = 7
	) AS AD on
	AD.No_ = I.[No_]
	WHERE EXISTS (SELECT V.[Item No_] from [VELO_NAV100CHLS_PROD].dbo.[VeloPlus$Item Variant] AS V (NOLOCK) WHERE I.[No_] = V.[Item No_] and V.[Webshop excluded]=0 
    and V.[Webshop approved]=1) and AD.Code is null
) AS [Source]
ON (Target.[No] = Source.[No] and Target.[AssortCode] = Source.[AssortCode])
WHEN MATCHED AND
(
	Target.[AssortSortingID] <> Source.[AssortSortingID]
)
THEN UPDATE SET     
   Target.[AssortSortingID] = Source.[AssortSortingID]
WHEN NOT MATCHED BY TARGET 
THEN INSERT (
   [No]
  ,[AssortCode]
  ,[AssortSortingID]      
 )
VALUES (
	Source.[No]
   ,Source.[AssortCode]
   ,Source.[AssortSortingID]  
   )
WHEN NOT MATCHED BY SOURCE THEN DELETE  
OUTPUT COALESCE(Inserted.No,Deleted.No),getdate() INTO @ChangeLog ;

INSERT INTO [dbo].[WS_Item_LOG] ([No],[Generated at],[Export State],[Exported at])
SELECT ItemNo, ChangedOn, 0, NULL
from @ChangeLog as cl
inner join [WS_Item_SET] item_set on item_set.[No] = cl.ItemNo
Where not exists(select * from [dbo].[WS_Item_LOG] ItemLog where ItemLog.[No]=cl.ItemNo and ItemLog.[Export State] = 0)
delete from @ChangeLog

--Item Attribute
MERGE WS_ItemAttr_SET AS [Target] 
USING (SELECT 
		I.[No_] as [No],
		A.[Link Field 2] AS [VarCode],
		A.[Attribute Code] AS [AttrCode],
		A.[Sequence] AS [AttrSequence],
		A.[Attribute Value] AS [AttrValue],
		'' AS [ATRAttrValue]
	FROM  [VELO_NAV100CHLS_PROD].dbo.[VeloPlus$Item] AS I (NOLOCK)
	INNER JOIN [VELO_NAV100CHLS_PROD].dbo.[VeloPlus$Attribute Value] AS A (NOLOCK) ON I.[No_] = A.[Link Field 1] 
	INNER JOIN [VELO_NAV100CHLS_PROD].dbo.[VeloPlus$Attribute] AS ATRB (NOLOCK) ON ATRB.[Code] = A.[Attribute Code]
	LEFT JOIN [VELO_NAV100CHLS_PROD].dbo.[VeloPlus$Attribute Option Value] AS AOV (NOLOCK) ON AOV.[Attribute Code] = A.[Attribute Code] and AOV.[Option Value] = A.[Attribute Value]
	WHERE EXISTS (SELECT V.[Item No_] from [VELO_NAV100CHLS_PROD].dbo.[VeloPlus$Item Variant] AS V (NOLOCK) WHERE I.[No_] = V.[Item No_] and V.[Webshop excluded]=0 and V.[Webshop approved]=1)
) AS [Source]
ON (Target.[No] = Source.[No] and Target.[VarCode] = Source.[VarCode] and Target.[AttrCode]=Source.[AttrCode] and Target.[AttrSequence]=Source.[AttrSequence])
WHEN MATCHED AND
(
	Target.[AttrValue] <> Source.[AttrValue]
	or Target.[ATRAttrValue] <> Source.[ATRAttrValue]
)
THEN UPDATE SET     
	Target.[AttrValue] = Source.[AttrValue],
	Target.[ATRAttrValue] = Source.[ATRAttrValue]
WHEN NOT MATCHED BY TARGET 
THEN INSERT (
	 [No]
    ,[VarCode]
    ,[AttrCode]
    ,[AttrSequence]
    ,[AttrValue]
    ,[ATRAttrValue]
 )
VALUES (
	Source.[No]
   ,Source.[VarCode]
   ,Source.[AttrCode]  
   ,Source.[AttrSequence]
   ,Source.[AttrValue]
   ,Source.[ATRAttrValue]
   )
WHEN NOT MATCHED BY SOURCE THEN DELETE  
OUTPUT COALESCE(Inserted.No,Deleted.No),getdate() INTO @ChangeLog ;

INSERT INTO [dbo].[WS_Item_LOG] ([No],[Generated at],[Export State],[Exported at])
SELECT ItemNo, ChangedOn, 0, NULL
from @ChangeLog as cl
inner join [WS_Item_SET] item_set on item_set.[No] = cl.ItemNo
Where not exists(select * from [dbo].[WS_Item_LOG] ItemLog where ItemLog.[No]=cl.ItemNo and ItemLog.[Export State] = 0)
delete from @ChangeLog

--Item HTML
MERGE WS_ItemHTML_SET AS [Target] 
USING (SELECT 
	I.[No_] as [No],
	'' as [LangCode],
	IVT.TextNo as [TextNo],
	IVT.[Text]as [Text]
	FROM [VELO_NAV100CHLS_PROD].dbo.[VeloPlus$Item] AS I (NOLOCK)
	INNER JOIN [VELO_NAV100CHLS_PROD].dbo.[VeloPlus$Item Variant Text] AS IVT (NOLOCK) ON IVT.[ItemNo] = I.[No_] and IVT.[LangCode] = ''
	INNER JOIN [VELO_NAV100CHLS_PROD].dbo.[VeloPlus$Extended Text Header] AS ETH (NOLOCK) ON ETH.[Table Name]=2 and ETH.No_ = I.[No_] and ETH.[Language Code] = '' and ETH.[Text No_] = IVT.TextNo and ETH.[Description]='Marketingtext'
       WHERE EXISTS (SELECT V.[Item No_] from [VELO_NAV100CHLS_PROD].dbo.[VeloPlus$Item Variant] AS V (NOLOCK) WHERE I.[No_] = V.[Item No_] and V.[Webshop excluded]=0 and V.[Webshop approved]=1)
) AS [Source]
ON (Target.[No] = Source.[No] and Target.[LangCode] = Source.[LangCode] and Target.[TextNo]=Source.[TextNo])
WHEN MATCHED AND
(
	Target.[Text] <> Source.[Text]
)
THEN UPDATE SET     
	Target.[Text] = Source.[Text]
WHEN NOT MATCHED BY TARGET 
THEN INSERT (
	 [No]
    ,[LangCode]
    ,[TextNo]
    ,[Text]
 )
VALUES (
	Source.[No]
   ,Source.[LangCode]
   ,Source.[TextNo]  
   ,Source.[Text]
   )
WHEN NOT MATCHED BY SOURCE THEN DELETE  
OUTPUT COALESCE(Inserted.No,Deleted.No),getdate() INTO @ChangeLog ;

INSERT INTO [dbo].[WS_Item_LOG] ([No],[Generated at],[Export State],[Exported at])
SELECT ItemNo, ChangedOn, 0, NULL
from @ChangeLog as cl
inner join [WS_Item_SET] item_set on item_set.[No] = cl.ItemNo
Where not exists(select * from [dbo].[WS_Item_LOG] ItemLog where ItemLog.[No]=cl.ItemNo and ItemLog.[Export State] = 0)
delete from @ChangeLog

--Item Picture
MERGE WS_ItemPicture_SET AS [Target] 
USING (SELECT 
		I.[No_] as [No],
		case 
    		when ExtMedia.[TableNo] = 10001413 then isnull(IVR.Variant, '')
  			ELSE isnull(ExtMedia.[Code 2], '')
  		END as [VarCode],
		ExtMedia.[PicFileName] as [PicFileName],
		ExtMedia.[Import Datetime] as [PicImportDatetime]
	FROM [VELO_NAV100CHLS_PROD].dbo.[VeloPlus$Item] AS I (NOLOCK)
	LEFT JOIN [VELO_NAV100CHLS_PROD].dbo.[VeloPlus$Item Variant Registration] AS IVR (NOLOCK) ON I.[No_] = IVR.[Item No_] 
	OUTER APPLY (SELECT TOP 1 [Value Description],[Add_ Value],[Add_ Value Description],[Add_ Value2],[Add_ Value2 Description],[Add_ Value3],[Add_ Value3 Description] 
				From [VELO_NAV100CHLS_PROD].dbo.[VeloPlus$Extended Variant Values] (NOLOCK)
				 WHERE [Item No_] = IVR.[Item No_] 
					and [Value] = IVR.[Variant Dimension 1] 
					and Dimension = 1 
					and Status = 0
				 ) AS VD1
	inner join (select [Item No_], 
					[Code 2],
					 case 
						when [Code 2] is null then ''
						when [Link Type] = 1 THEN [Picture Path] 
						ELSE [Picture Name] 
					  END as [PicFileName],
					max([Import Datetime]) [Import Datetime],
					[Table ID] as [TableNo]
				 from [VELO_NAV100CHLS_PROD].dbo.[VeloPlus$Extension Media] with(nolock)
				 where [Picture Type] <> 'HTML'
				 group by [Item No_], [Code 2], [Link Type], [Picture Name], [Picture Path], [Table ID]
				 ) as ExtMedia on ExtMedia.[Item No_] = I.[No_] and (([TableNo] = 10001413 and ExtMedia.[Code 2] = IVR.[Variant Dimension 1]) OR
                                (([TableNo] <> 10001413) and ((ExtMedia.[Code 2] = IVR.Variant) or (ExtMedia.[Code 2] = '' and IVR.Variant is null))))
	WHERE EXISTS (SELECT V.[Item No_] from [VELO_NAV100CHLS_PROD].dbo.[VeloPlus$Item Variant] AS V (NOLOCK) WHERE I.[No_] = V.[Item No_] and V.[Webshop excluded]=0 and V.[Webshop approved]=1)
) AS [Source]
ON (Target.[No] = Source.[No] and Target.[VarCode] = Source.[VarCode] and Target.[PicFileName]=Source.[PicFileName])
WHEN MATCHED AND
(
	Target.[PicImportDatetime] <> Source.[PicImportDatetime]
)
THEN UPDATE SET     
	Target.[PicImportDatetime] = Source.[PicImportDatetime]
WHEN NOT MATCHED BY TARGET 
THEN INSERT (
	 [No]
    ,[VarCode]
    ,[PicFileName]
    ,[PicImportDatetime]
 )
VALUES (
	Source.[No]
   ,Source.[VarCode]
   ,Source.[PicFileName]  
   ,Source.[PicImportDatetime]
   )
WHEN NOT MATCHED BY SOURCE THEN DELETE  
OUTPUT COALESCE(Inserted.No,Deleted.No),getdate() INTO @ChangeLog ;

INSERT INTO [dbo].[WS_Item_LOG] ([No],[Generated at],[Export State],[Exported at])
SELECT ItemNo, ChangedOn, 0, NULL
from @ChangeLog as cl
inner join [WS_Item_SET] item_set on item_set.[No] = cl.ItemNo
Where not exists(select * from [dbo].[WS_Item_LOG] ItemLog where ItemLog.[No]=cl.ItemNo and ItemLog.[Export State] = 0)
delete from @ChangeLog

--Item Sales Price
MERGE WS_ItemSalesPrice_SET AS [Target] 
USING (SELECT 
		I.[No_] as [No],
		V.Code as [VarCode],
		COALESCE(SP.[Unit Price Including VAT],COALESCE(SP_Item.[Unit Price Including VAT],0)) as [VarPriceInclVAT],
		COALESCE(SP.[Last Unit Price Before Promo],COALESCE(SP_Item.[Last Unit Price Before Promo],0)) as [VarPriceInclVATB4Promo],
		COALESCE(SP.[Promotion Type],COALESCE(SP_Item.[Promotion Type],0)) as [VarPromoType]
	FROM [VELO_NAV100CHLS_PROD].dbo.[VeloPlus$Item] AS I (NOLOCK)
	INNER JOIN [VELO_NAV100CHLS_PROD].dbo.[VeloPlus$Item Variant] AS V (NOLOCK) ON I.[No_] = V.[Item No_]
	OUTER APPLY (SELECT TOP 1 [Unit Price Including VAT], 
					[Promotion Type], 
					[Last Unit Price Before Promo] 
				 FROM [VELO_NAV100CHLS_PROD].dbo.[VeloPlus$Sales Price] (NOLOCK) 
				 WHERE [Item No_] = I.No_ 
					and [Sales Code] = 'ALL' 
					AND [Variant Code] = '' 
					AND [Starting Date] <= GETDATE() 
					AND ([Ending Date] > GETDATE()-1 
						Or [Ending Date] = '1753-01-01 00:00:00.000')
				    order by [Starting Date] desc) AS SP_Item
	OUTER APPLY (SELECT TOP 1 [Unit Price Including VAT], 
					[Promotion Type], 
					[Last Unit Price Before Promo] 
				 FROM [VELO_NAV100CHLS_PROD].dbo.[VeloPlus$Sales Price] (NOLOCK) 
				 WHERE [Item No_] = I.No_ 
					and [Sales Code] = 'ALL' 
					AND [Variant Code] = V.Code 
					AND [Starting Date] <= GETDATE() 
					AND ([Ending Date] > GETDATE()-1 
						Or [Ending Date] = '1753-01-01 00:00:00.000')
				    order by [Starting Date] desc) AS SP
        WHERE EXISTS (SELECT V.[Item No_] from [VELO_NAV100CHLS_PROD].dbo.[VeloPlus$Item Variant] AS V (NOLOCK) WHERE I.[No_] = V.[Item No_] and V.[Webshop excluded]=0 and V.[Webshop approved]=1)
) AS [Source]
ON (Target.[No] = Source.[No] and Target.[VarCode] = Source.[VarCode])
WHEN MATCHED AND
(
	Target.[VarPriceInclVAT] <> Source.[VarPriceInclVAT]
	or Target.[VarPriceInclVATB4Promo] <> Source.[VarPriceInclVATB4Promo]
	or Target.[VarPromoType] <> Source.[VarPromoType]
)
THEN UPDATE SET     
	Target.[VarPriceInclVAT] = Source.[VarPriceInclVAT],
	Target.[VarPriceInclVATB4Promo] = Source.[VarPriceInclVATB4Promo],
	Target.[VarPromoType] = Source.[VarPromoType]
WHEN NOT MATCHED BY TARGET 
THEN INSERT (
	 [No]
    ,[VarCode]
    ,[VarPriceInclVAT]
    ,[VarPriceInclVATB4Promo]
	,[VarPromoType]
 )
VALUES (
	Source.[No]
   ,Source.[VarCode]
   ,Source.[VarPriceInclVAT] 
   ,Source.[VarPriceInclVATB4Promo]
   ,Source.[VarPromoType]
   )
WHEN NOT MATCHED BY SOURCE THEN DELETE  
OUTPUT COALESCE(Inserted.No,Deleted.No),getdate() INTO @ChangeLog ;

INSERT INTO [dbo].[WS_Item_LOG] ([No],[Generated at],[Export State],[Exported at])
SELECT ItemNo, ChangedOn, 0, NULL
from @ChangeLog as cl
inner join [WS_Item_SET] item_set on item_set.[No] = cl.ItemNo
Where not exists(select * from [dbo].[WS_Item_LOG] ItemLog where ItemLog.[No]=cl.ItemNo and ItemLog.[Export State] = 0)
delete from @ChangeLog

--Item Sales Price
MERGE WS_ItemSalesPrice_SET AS [Target] 
USING (SELECT 
		I.[No_] as [No],
		V.Code as [VarCode],
		SP.[Unit Price Including VAT] as [VarPriceInclVAT],
		SP.[Last Unit Price Before Promo] as [VarPriceInclVATB4Promo],
		SP.[Promotion Type] as [VarPromoType]
	FROM [VELO_NAV100CHLS_PROD].dbo.[VeloPlus$Item] AS I (NOLOCK)
	INNER JOIN [VELO_NAV100CHLS_PROD].dbo.[VeloPlus$Item Variant] AS V (NOLOCK) ON I.[No_] = V.[Item No_]
	OUTER APPLY (SELECT TOP 1 [Unit Price Including VAT], 
					[Promotion Type], 
					[Last Unit Price Before Promo] 
				 FROM [VELO_NAV100CHLS_PROD].dbo.[VeloPlus$Sales Price] (NOLOCK) 
				 WHERE [Item No_] = I.No_ 
					and [Sales Code] = 'ALL' 
					AND [Variant Code] = V.Code 
					AND [Starting Date] <= GETDATE() 
					AND ([Ending Date] > GETDATE()-1 
						Or [Ending Date] = '1753-01-01 00:00:00.000')) AS SP
        WHERE EXISTS (SELECT V.[Item No_] from [VELO_NAV100CHLS_PROD].dbo.[VeloPlus$Item Variant] AS V (NOLOCK) WHERE I.[No_] = V.[Item No_] and V.[Webshop excluded]=0 and V.[Webshop approved]=1)
) AS [Source]
ON (Target.[No] = Source.[No] and Target.[VarCode] = Source.[VarCode])
WHEN MATCHED AND
(
	Target.[VarPriceInclVAT] <> Source.[VarPriceInclVAT]
	or Target.[VarPriceInclVATB4Promo] <> Source.[VarPriceInclVATB4Promo]
	or Target.[VarPromoType] <> Source.[VarPromoType]
)
THEN UPDATE SET     
	Target.[VarPriceInclVAT] = Source.[VarPriceInclVAT],
	Target.[VarPriceInclVATB4Promo] = Source.[VarPriceInclVATB4Promo],
	Target.[VarPromoType] = Source.[VarPromoType]
WHEN NOT MATCHED BY TARGET 
THEN INSERT (
	 [No]
    ,[VarCode]
    ,[VarPriceInclVAT]
    ,[VarPriceInclVATB4Promo]
	,[VarPromoType]
 )
VALUES (
	Source.[No]
   ,Source.[VarCode]
   ,Source.[VarPriceInclVAT] 
   ,Source.[VarPriceInclVATB4Promo]
   ,Source.[VarPromoType]
   )
WHEN NOT MATCHED BY SOURCE THEN DELETE  
OUTPUT COALESCE(Inserted.No,Deleted.No),getdate() INTO @ChangeLog ;

INSERT INTO [dbo].[WS_Item_LOG] ([No],[Generated at],[Export State],[Exported at])
SELECT ItemNo, ChangedOn, 0, NULL
from @ChangeLog as cl
inner join [WS_Item_SET] item_set on item_set.[No] = cl.ItemNo
Where not exists(select * from [dbo].[WS_Item_LOG] ItemLog where ItemLog.[No]=cl.ItemNo and ItemLog.[Export State] = 0)
delete from @ChangeLog

--Item Translation
MERGE WS_ItemTranslation_SET AS [Target] 
USING (SELECT 
		I.[No_] as [No],
		IT.[Variant Code] as [VarCode],
		iif(IT.[Language Code]= '','DES', IT.[Language Code]) AS [LangCode],
		IT.[Description] AS [Description],
		IT.[Description 2] AS [Description2],
		IT.[Webshop Description] AS [ShopTitle]
	FROM [VELO_NAV100CHLS_PROD].dbo.[VeloPlus$Item] AS I (NOLOCK)
	INNER JOIN [VELO_NAV100CHLS_PROD].dbo.[VeloPlus$Item Variant] AS V (NOLOCK) ON I.[No_] = V.[Item No_]
	INNER JOIN [VELO_NAV100CHLS_PROD].dbo.[VeloPlus$Item Translation] AS IT (NOLOCK) ON I.[No_] = IT.[Item No_]
	WHERE EXISTS (SELECT V.[Item No_] from [VELO_NAV100CHLS_PROD].dbo.[VeloPlus$Item Variant] AS V (NOLOCK) WHERE I.[No_] = V.[Item No_] and V.[Webshop excluded]=0 and V.[Webshop approved]=1)
) AS [Source]
ON (Target.[No] = Source.[No] and Target.[VarCode] = Source.[VarCode] and Target.[LangCode] = Source.[LangCode])
WHEN MATCHED AND
(
	Target.[Description] <> Source.[Description]
	or Target.[Description2] <> Source.[Description2]
	or Target.[ShopTitle] <> Source.[ShopTitle]
)
THEN UPDATE SET     
	Target.[Description] = Source.[Description],
	Target.[Description2] = Source.[Description2],
	Target.[ShopTitle] = Source.[ShopTitle]
WHEN NOT MATCHED BY TARGET 
THEN INSERT (
	 [No]
    ,[VarCode]
    ,[LangCode]
    ,[Description]
	,[Description2]
	,[ShopTitle]
 )
VALUES (
	Source.[No]
   ,Source.[VarCode]
   ,Source.[LangCode] 
   ,Source.[Description]
   ,Source.[Description2]
   ,Source.[ShopTitle]
   )
WHEN NOT MATCHED BY SOURCE THEN DELETE  
OUTPUT COALESCE(Inserted.No,Deleted.No),getdate() INTO @ChangeLog ;

INSERT INTO [dbo].[WS_Item_LOG] ([No],[Generated at],[Export State],[Exported at])
SELECT ItemNo, ChangedOn, 0, NULL
from @ChangeLog as cl
inner join [WS_Item_SET] item_set on item_set.[No] = cl.ItemNo
Where not exists(select * from [dbo].[WS_Item_LOG] ItemLog where ItemLog.[No]=cl.ItemNo and ItemLog.[Export State] = 0)
delete from @ChangeLog

--Item Variant
MERGE WS_ItemVar_SET AS [Target] 
USING (SELECT 
		I.[No_] as [No],
		V.[Code] AS [VarCode],
		V.[Description] AS [Description],
		V.[Description 2] AS [Description2],
		V.[Common Item No_] AS [CommonItemNo],
		V.[Allocation Key] AS [AllocationKey],
		isnull(B.[Barcode No_],'') AS [Barcode],
		isnull(nullif(IVR.[Season Code], ''), I.[Season Code]) AS [SeasonCode], 
		isnull(VD1.[Value Description],'') AS [Dim1],
		isnull(VD1.[Add_ Value],'') AS [Dim1AV1],
		isnull(VD1.[Add_ Value2],'') AS [Dim1AV2],
		isnull(VD1.[Add_ Value3],'') AS [Dim1AV3],
		isnull(VD1.[Add_ Value Description],'') AS [Dim1AVD1],
		isnull(VD1.[Add_ Value2 Description],'') AS [Dim1AVD2],
		isnull(VD1.[Add_ Value3 Description],'') AS [Dim1AVD3],
		isnull(VD2.[Value Description],'') AS [Dim2],
		isnull(VD2.[Logical Order],'') AS [Dim2LogicalOrder]
	FROM [VELO_NAV100CHLS_PROD].dbo.[VeloPlus$Item] AS I (NOLOCK)
	INNER JOIN [VELO_NAV100CHLS_PROD].dbo.[VeloPlus$Item Variant] AS V (NOLOCK) ON I.[No_] = V.[Item No_] and V.[Webshop excluded]=0 and V.[Webshop approved]=1
	LEFT JOIN [VELO_NAV100CHLS_PROD].dbo.[VeloPlus$Item Variant Registration] AS IVR (NOLOCK) ON I.[No_] = IVR.[Item No_] 
																	and IVR.Variant = V.Code 
	OUTER APPLY (SELECT TOP 1 [Value Description],[Add_ Value],[Add_ Value Description],[Add_ Value2],[Add_ Value2 Description],[Add_ Value3],[Add_ Value3 Description] 
				From [VELO_NAV100CHLS_PROD].dbo.[VeloPlus$Extended Variant Values] (NOLOCK)
				 WHERE [Item No_] = IVR.[Item No_] 
					and [Value] = IVR.[Variant Dimension 1] 
					and Dimension = 1 
					and Status = 0
				 ) AS VD1
	OUTER APPLY (SELECT TOP 1 [Value Description],[Logical Order]
				 From [VELO_NAV100CHLS_PROD].dbo.[VeloPlus$Extended Variant Values] (NOLOCK)
				 WHERE [Item No_] = IVR.[Item No_] 
					and [Value] = IVR.[Variant Dimension 2] 
					and Dimension = 2 
					and Status = 0
				 ) AS VD2
	OUTER APPLY (SELECT TOP 1 [Barcode No_] 
				FROM [VELO_NAV100CHLS_PROD].dbo.[VeloPlus$Barcodes] (NOLOCK)
				 WHERE [Item No_] = I.No_ 
					and [Variant Code] = V.[Code] 
				 Order By [Show for Item] Desc
				 ) AS B
) AS [Source]
ON (Target.[No] = Source.[No] and Target.[VarCode] = Source.[VarCode])
WHEN MATCHED AND
(
     Target.[Description] <> Source.[Description]
     or Target.[Description2] <> Source.[Description2]
     or Target.[CommonItemNo] <> Source.[CommonItemNo]
     or Target.[AllocationKey] <> Source.[AllocationKey]
     or Target.[Barcode] <> Source.[Barcode]
     or Target.[SeasonCode] <> Source.[SeasonCode]
     or Target.[Dim1] <> Source.[Dim1]
     or Target.[Dim1AV1] <> Source.[Dim1AV1]
     or Target.[Dim1AV2] <> Source.[Dim1AV2]
     or Target.[Dim1AV3] <> Source.[Dim1AV3]
     or Target.[Dim1AVD1] <> Source.[Dim1AVD1]
     or Target.[Dim1AVD2] <> Source.[Dim1AVD2]
     or Target.[Dim1AVD3] <> Source.[Dim1AVD3]
     or Target.[Dim2] <> Source.[Dim2]
     or Target.[Dim2LogicalOrder] <> Source.[Dim2LogicalOrder]
)
THEN UPDATE SET     
     Target.[Description] = Source.[Description],
     Target.[Description2] = Source.[Description2],
     Target.[CommonItemNo] = Source.[CommonItemNo],
     Target.[AllocationKey] = Source.[AllocationKey],
     Target.[Barcode] = Source.[Barcode],
     Target.[SeasonCode] = Source.[SeasonCode],
     Target.[Dim1] = Source.[Dim1],
     Target.[Dim1AV1] = Source.[Dim1AV1],
     Target.[Dim1AV2] = Source.[Dim1AV2],
     Target.[Dim1AV3] = Source.[Dim1AV3],
     Target.[Dim1AVD1] = Source.[Dim1AVD1],
     Target.[Dim1AVD2] = Source.[Dim1AVD2],
     Target.[Dim1AVD3] = Source.[Dim1AVD3],
     Target.[Dim2] = Source.[Dim2],
     Target.[Dim2LogicalOrder] = Source.[Dim2LogicalOrder]
WHEN NOT MATCHED BY TARGET 
THEN INSERT (
	 [No]
     ,[VarCode]
     ,[Description]
     ,[Description2]
     ,[CommonItemNo]
     ,[AllocationKey]
     ,[Barcode]
     ,[SeasonCode]
     ,[Dim1]
     ,[Dim1AV1]
     ,[Dim1AV2]
     ,[Dim1AV3]
     ,[Dim1AVD1]
     ,[Dim1AVD2]
     ,[Dim1AVD3]
     ,[Dim2]
     ,[Dim2LogicalOrder]
 )
VALUES (
	 Source.[No]
     ,Source.[VarCode]
     ,Source.[Description]
     ,Source.[Description2]
     ,Source.[CommonItemNo]
     ,Source.[AllocationKey]
     ,Source.[Barcode]
     ,Source.[SeasonCode]
     ,Source.[Dim1]
     ,Source.[Dim1AV1]
     ,Source.[Dim1AV2]
     ,Source.[Dim1AV3]
     ,Source.[Dim1AVD1]
     ,Source.[Dim1AVD2]
     ,Source.[Dim1AVD3]
     ,Source.[Dim2]
     ,Source.[Dim2LogicalOrder]
   )
WHEN NOT MATCHED BY SOURCE THEN DELETE  
OUTPUT COALESCE(Inserted.No,Deleted.No),getdate() INTO @ChangeLog ;

INSERT INTO [dbo].[WS_Item_LOG] ([No],[Generated at],[Export State],[Exported at])
SELECT ItemNo, ChangedOn, 0, NULL
from @ChangeLog as cl
inner join [WS_Item_SET] item_set on item_set.[No] = cl.ItemNo
Where not exists(select * from [dbo].[WS_Item_LOG] ItemLog where ItemLog.[No]=cl.ItemNo and ItemLog.[Export State] = 0)
delete from @ChangeLog

COMMIT;

---------------------------
--OK
---------------------------

