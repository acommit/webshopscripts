USE Webshop_PROD
GO
--NAV 2017 PROD for Velo+ - powered by Acommit
---------------------------
select top 100 * FROM dbo.[WS_ItemSalesPrice_SET]
Where No = '33000022'

update item_log
set [Export State] = 0 --reset export state if case the export crashed previously
from dbo.[WS_Item_LOG] item_log
where [Export State] = 9

update top (750) item_log 
set [Export State] = 9
from dbo.[WS_Item_LOG] as item_log 
where [Export State] = 0

declare @item Table ([ItemNo] nvarchar(20))
insert into @item 
SELECT item_log.[No] from dbo.[WS_Item_LOG] item_log
where [Export State] = 9
group by [No]

select  getdate() as [GeneratedAt], (
select 
	[Item.No], 
	[Item.Description] as [Item.Description], 
	[Item.Description2] as [Item.Description2],
	[Item.ShopTitle] as [Item.ShopTitle], 
	[Item.SalesUnitofMeasure],
	[Item.VendorItemNo],
	[Item.VendorNo],
	[Item.ItemFamilyCode],
	[Item.ItemFamilyDescr],
	[Item.LastDateModified],
	[Item.PicturesUpdated],
	[Item.HTMLTexts],
	[Item.Pictures],
    [Item.Assortments],
	[Item.Attributes],
	[Item.Variants]
from (
	select
		[Item].[No]	as [Item.No],
        (SELECT [Item].[Description] as 'DES' FOR JSON PATH) as 'Item.Description',
        (SELECT [Item].[Description2] as 'DES' FOR JSON PATH) as 'Item.Description2',
        (SELECT [Item].[ShopTitle] as 'DES' FOR JSON PATH) as 'Item.ShopTitle',

		[Item].[SalesUnitofMeasure] as [Item.SalesUnitofMeasure],
        [Item].[VendorItemNo] as [Item.VendorItemNo],
        [Item].[VendorNo] as [Item.VendorNo],
        [Item].[ItemFamilyCode] as [Item.ItemFamilyCode],
		(select [Item].[IFDescription] as 'DES', '' as 'FRS' for json path) as 'Item.ItemFamilyDescr',
		[Item].[LastDateModified] as [Item.LastDateModified],
        CASE WHEN 
			[Picturesupdated] = 1 THEN 'True' 
			ELSE 'False' 
		END as [Item.PicturesUpdated],
		
		(Select distinct
			CASE 
				WHEN LangCode = '' THEN 'DES' 
				ELSE LangCode 
			END as [HTMLText.LangCode], 			
			TextNo as [HTMLText.TextNo], 
			[Text] as [HTMLText.Text]
		from [WS_ItemHTML_SET] [Set_HTML]
		where [Set_HTML].[No] = [Item].[No]
		  and [Set_HTML].[Text] <> ''
		FOR JSON PATH
		) as [Item.HTMLTexts],
		
		(Select distinct			
			AssortCode as [Assortment.Code], 
			AssortSortingID as [Assortment.SortingID]
		from [WS_ItemAssortment_SET] [Set_Assort]
		where [Set_Assort].[No] = [Item].[No]
		FOR JSON PATH
		) as [Item.Assortments],		
		(Select distinct 
			[PicFileName] as [Picture.FileName],
			format([PicImportDatetime],'yyyyMMddHHmmss') as [Picture.ImportDateTime]
        from [WS_ItemPicture_SET] [Set_Pic]
        where [Set_Pic].[No] = [Item].[No]
			and [VarCode] = '' and PicFileName <> ''
        FOR JSON PATH
		) as [Item.Pictures],
		(
		Select  distinct 
			AttrCode as [Attribute.Code], 
			[Attribute].[AttrValue] as [Attribute.Value.DES]
        from [WS_ItemAttr_SET] [Attribute]
        where [Attribute].[No] = [Item].[No]
			and [Attribute].[VarCode] = ''
			and [Attribute].[AttrValue] <> ''
        FOR JSON PATH
		) as [Item.Attributes],
		(
		select [Variant.Code],
			[Variant.Description], 
			[Variant.Description2],
			[Variant.Dim1.DES],
			[Variant.Dim2.DES],
			[Variant.Dim2LOrder],
			[Variant.CommonItemNo],
			[Variant.UnitPriceIncludingVAT],
			[Variant.PromoType],
			[Variant.PriceInclVATB4Promo],
			[Variant.SeasonCode],
			[Variant.AllocKey],
			[Variant.EAN],
			[Variant.Attributes],
			[Variant.Pictures]
		from (
			SELECT [Variant].VarCode as [Variant.Code],
				(select [Variant].[Description] as 'DES' for json path) as [Variant.Description],
				(select [Variant].[Description2] as 'DES' for json path) as [Variant.Description2],

				[Variant].Dim1 as [Variant.Dim1.DES],
				[Variant].Dim2 as [Variant.Dim2.DES],
				[Variant].Dim2LogicalOrder as [Variant.Dim2LOrder],
				[Variant].CommonItemNo as [Variant.CommonItemNo],
				FORMAT([ItemPrice].VarPriceInclVAT,'N2') as [Variant.UnitPriceIncludingVAT],
				[ItemPrice].VarPromoType as [Variant.PromoType],
				FORMAT([ItemPrice].VarPriceInclVATB4Promo,'N2') as [Variant.PriceInclVATB4Promo],
				[Variant].SeasonCode as [Variant.SeasonCode],
				[Variant].AllocationKey as [Variant.AllocKey],
				[Variant].Barcode as [Variant.EAN],
				(
				Select distinct
					AttrCode as [Attribute.Code], 
					case when 
						AttrCode = 'HIST_ARTIKELGRP_ID' THEN REPLACE(REPLACE(AttrValue,'''',''),' ','') 
						ELSE AttrValue 
					END as [Attribute.Value.DES]
				from (select [No],[VarCode],AttrCode,AttrValue
					from dbo.[WS_ItemAttr_SET] [Attribute] 
					where [Attribute].[No] = [Variant].[No]
					and [Attribute].[VarCode] = [Variant].[VarCode]
					and [Attribute].[AttrValue] <> ''
					UNION
					Select [No],VarCode,'ALLG_COLOR1',Dim1AVD1 + '#' + Dim1AV1 from dbo.[WS_ItemVar_SET] [Attribute] 
					where [Attribute].[No] = [Variant].[No]
					and [Attribute].[VarCode] = [Variant].[VarCode]
					and [Attribute].[Dim1AVD1] <> '' and [Attribute].[Dim1AV1] <> '' 
					UNION
					Select [No],VarCode,'ALLG_COLOR2',Dim1AVD2 + '#' + Dim1AV2 from dbo.[WS_ItemVar_SET] [Attribute] 
					where [Attribute].[No] = [Variant].[No]
					and [Attribute].[VarCode] = [Variant].[VarCode]
					and [Attribute].[Dim1AVD2] <> '' and [Attribute].[Dim1AV2] <> '' 
					UNION
					Select [No],VarCode,'ALLG_COLOR3',Dim1AVD3 + '#' + Dim1AV3 from dbo.[WS_ItemVar_SET] [Attribute] 
					where [Attribute].[No] = [Variant].[No]
					and [Attribute].[VarCode] = [Variant].[VarCode]
					and [Attribute].[Dim1AVD1] <> '' and [Attribute].[Dim1AV3] <> '' 
				) as VarAttr
				FOR JSON PATH
				) as [Variant.Attributes],
				(
				Select distinct 
					[PicFileName] as [Picture.Filename],
					format([PicImportDatetime],'yyyyMMddHHmmss') as [Picture.ImportDateTime]
				from dbo.[WS_ItemPicture_SET] [Set_Pic]
				where [Set_Pic].[No] = [Variant].[No]
					and [Set_Pic].[VarCode] = [Variant].[VarCode] and PicFileName <> ''
				FOR JSON PATH
				) as [Variant.Pictures]
			from dbo.[WS_ItemVar_SET] as [Variant]
			inner join dbo.[WS_ItemSalesPrice_SET] as [ItemPrice] on
					[ItemPrice].[No] = [Variant].[No]
					and [ItemPrice].[VarCode] = [Variant].[VarCode]
			where [Variant].[No] = [Item].[No]
			) xy
		group by [Variant.Code],
			[Variant.Dim1.DES],
			[Variant.Dim2.DES],
			[Variant.Dim2LOrder],
			[Variant.CommonItemNo],
			[Variant.UnitPriceIncludingVAT],
			[Variant.PromoType],
			[Variant.PriceInclVATB4Promo],
			[Variant.SeasonCode],
			[Variant.AllocKey],
			[Variant.EAN],
			[Variant.Attributes],
			[Variant.Pictures],
            [Variant.Description],
			[Variant.Description2]
		FOR JSON path
		) as [Item.Variants] 
	from [WS_Item_SET] [Item]
	inner join @item item_log on item_log.[ItemNo] = [No]
	) xy
FOR JSON path
) Items
for json path, root('Result')

---------------------------
--OK
---------------------------