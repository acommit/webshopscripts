select isnull(
    (select distinct 
    T.[Code] as 'Assortment.Code',
    T.[AttachedtoCode] as 'Assortment.AttachedtoCode',
    (SELECT T.[Description] as 'DES',
    (select distinct TranslationDescription from %1.dbo.[ItemAssortment_SET] where [Code] = T.[Code] AND TranslationLanguageCode = 'FRS') AS 'FRS' FOR JSON PATH) as 'Assortment.Description',
    (SELECT T.[Description2] as 'DES',
    (select distinct TranslationDescription2 from %1.dbo.[ItemAssortment_SET] where [Code] = T.[Code] AND TranslationLanguageCode = 'FRS') AS 'FRS' FOR JSON PATH) as 'Assortment.Description2',
    T.[CodingText] as 'Assortment.CodingText',
    T.[ECommerceGroups] as 'Assortment.Filter',
    T.SortingID as 'Assortment.SortingID',
    T.PurchaserName as 'Assortment.PurchaserName',
    T.PurchaserEMail as 'Assortment.PurchaserEMail'
from %1.dbo.[Export Data Log] as [Log] 
inner join %1.dbo.[ItemAssortment_LOG] as tl on [Log].[Batch No_] = tl.[Batch No_] 
inner join %1.dbo.[ItemAssortment_SET] as T on tl.[Code] = T.[Code]  and tl.[AttachedtoCode] = T.[AttachedtoCode] and tl.[TranslationLanguageCode] = T.[TranslationLanguageCode]  
where [Log].[Packages Code] = 'WS_EXPORT' 
  and [Log].[Set Name] = 'ASSORTMENT' 
  and [WhereCondition] 
ORDER BY T.SortingID, T.Code  
 for JSON PATH, ROOT('ItemAssortments')), '') as [Result]  
--%1
update Log 
set [Exported] = 1, 
    [Exported at] = GetDate() 
from %1.dbo.[Export Data Log] [Log] 
where [Log].[Packages Code] = 'WS_EXPORT' 
  and [Log].[Set Name] = 'ASSORTMENT' 
  and [WhereCondition] 