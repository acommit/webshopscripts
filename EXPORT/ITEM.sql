
USE %1
-- reset items for the full export -W_bshop_TEST

IF OBJECT_ID('tempdb..#Item') IS NOT NULL
--PRINT 'EXISTS!'
DROP TABLE #Item;

IF '[WhereCondition]' = '[Target_Log].[Export State] = 1' --NAV
--OR 1 = 1 --DEBUG Full Export
BEGIN
update il set il.[Export State] = 1 
  from dbo.[Item_LOG] as il
  where il.[Export State] in (0,9)
END

;with
    il_cte
    as
    (
        select [Target_Log].*
        from dbo.[Export Data Log] as [Log]
            inner join dbo.[Item_LOG] as [Target_Log]
            on [Log].[Batch No_] = [Target_Log].[Batch No_] and [Log].[Packages Code] = 'WS_EXPORT' and [Log].[Set Name] = 'ITEM'
            inner join dbo.[Item_SET] as [Target]
            on [Target_Log].[No] = [Target].[No] and [Target_Log].[IHTLangCode] = [Target].[IHTLangCode] and [Target_Log].[IHTTextNo] = [Target].[IHTTextNo] and [Target_Log].[ITLangCode] = [Target].[ITLangCode] and [Target_Log].[ITVariantCode] = [Target].[ITVariantCode] and [Target_Log].[VarCode] = [Target].[VarCode] and [Target_Log].[VarBarcode] = [Target].[VarBarcode] and [Target_Log].[AttrCode] = [Target].[AttrCode] and [Target_Log].[AttrVariant] = [Target].[AttrVariant] and [Target_Log].[AttrSequence] = [Target].[AttrSequence] and [Target_Log].[VarDim1] = [Target].[VarDim1] and [Target_Log].[VarDim2] = [Target].[VarDim2] and [Target_Log].[VarDim2LogicalOrder] = [Target].[VarDim2LogicalOrder] and [Target_Log].[AssortCode] = [Target].[AssortCode]
        where [WhereCondition] --NAV
        --where [Target_Log].[Export State] = 10
        --DEBUG Full Export
    )
update il set il.[Export State] = 0 
from dbo.[Item_LOG] as il
    inner join (select Distinct MAX(il_cte.[Batch No_]) as [Batch No_], [No], [IHTLangCode], [IHTTextNo], [ITLangCode], [ITVariantCode], [VarCode], [VarBarcode], [AttrCode], [AttrVariant], [AttrSequence], [VarDim1], [VarDim2], [VarDim2LogicalOrder], [AssortCode]
    from il_cte
    group by [No],[IHTLangCode],[IHTTextNo],[ITLangCode],[ITVariantCode],[VarCode],[VarBarcode],[AttrCode],[AttrVariant],[AttrSequence],[VarDim1],[VarDim2],[VarDim2LogicalOrder],[AssortCode]
            ) as ml on il.[Batch No_] = ml.[Batch No_] and il.[No] = ml.[No] AND il.[IHTLangCode] = ml.[IHTLangCode] AND il.[IHTTextNo] = ml.[IHTTextNo] AND il.[ITLangCode] = ml.[ITLangCode] AND il.[ITVariantCode] = ml.[ITVariantCode] AND il.[VarCode] = ml.[VarCode] AND il.[VarBarcode] = ml.[VarBarcode] AND il.[AttrCode] = ml.[AttrCode] AND il.[AttrVariant] = ml.[AttrVariant] AND il.[AttrSequence] = ml.[AttrSequence] AND il.[VarDim1] = ml.[VarDim1] AND il.[VarDim2] = ml.[VarDim2] AND il.[VarDim2LogicalOrder] = ml.[VarDim2LogicalOrder] AND il.[AssortCode] = ml.[AssortCode]

IF '[WhereCondition]' = '[Target_Log].[Export State] = 1' --Full export case
--OR 1 = 1 --DEBUG Full Export
    GOTO EndOfFile;

update il
set [Export State] = 0 --reset export state if case the export crashed previously
from dbo.[Item_LOG] il
where [Export State] = 9

declare @Batch Table ([ItemNo] nvarchar(20))
insert into @Batch
select distinct TOP [TopExpression] [Target_Log].[No]
from dbo.[Export Data Log] as [Log]
    inner join dbo.[Item_LOG] as [Target_Log] on [Log].[Batch No_] = [Target_Log].[Batch No_] and [Log].[Packages Code] = 'WS_EXPORT' and [Log].[Set Name] = 'ITEM' 
    and [Target_Log].[Export State] = 0
    inner join dbo.[Item_SET] as [Target] on [Target_Log].[No] = [Target].[No] and [Target_Log].[IHTLangCode] = [Target].[IHTLangCode] and [Target_Log].[IHTTextNo] = [Target].[IHTTextNo] and [Target_Log].[ITLangCode] = [Target].[ITLangCode] and [Target_Log].[ITVariantCode] = [Target].[ITVariantCode] and [Target_Log].[VarCode] = [Target].[VarCode] and [Target_Log].[VarBarcode] = [Target].[VarBarcode] and [Target_Log].[AttrCode] = [Target].[AttrCode] and [Target_Log].[AttrVariant] = [Target].[AttrVariant] and [Target_Log].[AttrSequence] = [Target].[AttrSequence] and [Target_Log].[VarDim1] = [Target].[VarDim1] and [Target_Log].[VarDim2] = [Target].[VarDim2] and [Target_Log].[VarDim2LogicalOrder] = [Target].[VarDim2LogicalOrder] and [Target_Log].[AssortCode] = [Target].[AssortCode]

update [Target_Log] 
set [Export State] = 9
from dbo.[Export Data Log] as [Log]
    inner join dbo.[Item_LOG] as [Target_Log] on [Log].[Packages Code] = 'WS_EXPORT' and [Log].[Set Name] = 'ITEM' and [Log].[Batch No_] = [Target_Log].[Batch No_] --and [Target_Log].[Export State] = 0
    inner join dbo.[Item_SET] as [Target] on [Target_Log].[No] = [Target].[No] and [Target_Log].[IHTLangCode] = [Target].[IHTLangCode] and [Target_Log].[IHTTextNo] = [Target].[IHTTextNo] and [Target_Log].[ITLangCode] = [Target].[ITLangCode] and [Target_Log].[ITVariantCode] = [Target].[ITVariantCode] and [Target_Log].[VarCode] = [Target].[VarCode] and [Target_Log].[VarBarcode] = [Target].[VarBarcode] and [Target_Log].[AttrCode] = [Target].[AttrCode] and [Target_Log].[AttrVariant] = [Target].[AttrVariant] and [Target_Log].[AttrSequence] = [Target].[AttrSequence] and [Target_Log].[VarDim1] = [Target].[VarDim1] and [Target_Log].[VarDim2] = [Target].[VarDim2] and [Target_Log].[VarDim2LogicalOrder] = [Target].[VarDim2LogicalOrder] and [Target_Log].[AssortCode] = [Target].[AssortCode]
    inner join @Batch as GroupSet on [Target_Log].No = GroupSet.[ItemNo];

select distinct [Target].[No],
		[Target].ITLangCode,
		[Target].[Description],
		[Target].[Description2],
		[Target].[ShopTitle],
		[Target].[SalesUnitofMeasure],
        [Target].[VendorItemNo],
        [Target].[VendorNo],
        [Target].[ItemFamilyCode],
		[Target].[IFDescription],
		[Target].IFTDescription,	
		[Target].[LastDateModified],
        [Target].[Picturesupdated],
		[Target].IHTTextNo,
		[Target].[IHTText],
		[Target].AssortCode, 
		[Target].AssortSortingID,
		[Target].[PicFileName],
		[Target].[PicImportDatetime],
        [Target].[PicItemVariant],
		[Target].[AttrItemVariant],
		[Target].AttrCode,
		[Target].[AttrValue]
into #Item
from dbo.[Export Data Log] as [Log]
inner join dbo.[Item_LOG] as [Target_Log] on [Log].[Batch No_] = [Target_Log].[Batch No_] 
inner join dbo.[Item_SET] as [Target] on [Target_Log].[No] = [Target].[No]  and [Target_Log].[IHTLangCode] = [Target].[IHTLangCode]  and [Target_Log].[IHTTextNo] = [Target].[IHTTextNo]  and [Target_Log].[ITLangCode] = [Target].[ITLangCode]  and [Target_Log].[ITVariantCode] = [Target].[ITVariantCode]  and [Target_Log].[VarCode] = [Target].[VarCode]  and [Target_Log].[VarBarcode] = [Target].[VarBarcode]  and [Target_Log].[AttrCode] = [Target].[AttrCode]  and [Target_Log].[AttrVariant] = [Target].[AttrVariant]  and [Target_Log].[AttrSequence] = [Target].[AttrSequence]  and [Target_Log].[VarDim1] = [Target].[VarDim1]  and [Target_Log].[VarDim2] = [Target].[VarDim2]  and [Target_Log].[VarDim2LogicalOrder] = [Target].[VarDim2LogicalOrder]  and [Target_Log].[AssortCode] = [Target].[AssortCode] 
                                    and ([Target].[PicItemVariant] = '' 
                                        or [Target].[AttrItemVariant] = ''
                                        or [Target].[VarCode] = '')
where [Log].[Packages Code] = 'WS_EXPORT'
    and [Log].[Set Name] = 'ITEM'
    and [Target_Log].[Export State] = 9 
 
select  getdate() as [GeneratedAt], (
select 
	[Item.No], 
	[Item.Description] as [Item.Description], 
	[Item.Description2] as [Item.Description2],
	[Item.ShopTitle] as [Item.ShopTitle], 
	[Item.SalesUnitofMeasure],
	[Item.VendorItemNo],
	[Item.VendorNo],
	[Item.ItemFamilyCode],
	[Item.ItemFamilyDescr],
	[Item.LastDateModified],
	[Item.PicturesUpdated],
	[Item.HTMLTexts],
	[Item.Pictures],
	[Item.Assortments],
	[Item.Attributes],
	[Item.Variants]
from (
	select
		[Set].[No]	as [Item.No],	
		-- (select case when ITLangCode in ('DES', '') then [Set].[Description] end as 'DES',
		-- case when ITLangCode = 'FRS' then [Set].[Description] end as 'FRS') as [Item.Description],
        (SELECT case when ITLangCode in ('DES', '') then [Set].[Description] end as 'DES' FOR JSON PATH) as 'Item.Description',
        (SELECT case when ITLangCode in ('DES', '') then [Set].[Description2] end as 'DES' FOR JSON PATH) as 'Item.Description2',
        (SELECT case when ITLangCode in ('DES', '') then [Set].[ShopTitle] end as 'DES' FOR JSON PATH) as 'Item.ShopTitle',

		[SalesUnitofMeasure] as [Item.SalesUnitofMeasure],
        [VendorItemNo] as [Item.VendorItemNo],
        [VendorNo] as [Item.VendorNo],
        [ItemFamilyCode] as [Item.ItemFamilyCode],

		(select case when ITLangCode in ('DES', '') then [Set].[IFDescription] end as 'DES', '' as 'FRS' for json path) as 'Item.ItemFamilyDescr',
		[LastDateModified] as [Item.LastDateModified],
        CASE WHEN [Picturesupdated] = 1 THEN 'True' ELSE 'False' END as [Item.PicturesUpdated],

		(Select distinct
			CASE 
				WHEN ITLangCode = '' THEN 'DES' 
				ELSE ITLangCode 
			END as [HTMLText.LangCode], 			
			IHTTextNo as [HTMLText.TextNo], 
			[IHTText] as [HTMLText.Text]
		from #Item [Set_HTML]
		where [Set_HTML].[No] = [Set].[No]
		  and [Set_HTML].[IHTText] <> ''
		FOR JSON PATH
		) as [Item.HTMLTexts],

		(Select distinct			
			AssortCode as [Assortment.Code], 
			AssortSortingID as [Assortment.SortingID]
		from #Item [Set_Assort]
		where [Set_Assort].[No] = [Set].[No]
		FOR JSON PATH
		) as [Item.Assortments],

		(Select distinct 
			[PicFileName] as [Picture.FileName],
			format([PicImportDatetime],'yyyyMMddHHmmss') as [Picture.ImportDateTime]
        from #Item [Set_Pic]
        where [Set_Pic].[No] = [Set].[No]
			and [PicItemVariant] = '' and PicFileName <> ''
        FOR JSON PATH
		) as [Item.Pictures],
		(
		Select  distinct 
			AttrCode as [Attribute.Code], 
			case when ITLangCode in ('DES', '') then [Set_Attribute].[AttrValue] end as [Attribute.Value.DES],
			case when ITLangCode = 'FRS' then [Set_Attribute].[AttrValue] end as [Attribute.Value.FRS]
        from  #Item [Set_Attribute]
        where [Set_Attribute].[No] = [Set].[No]
			and [Set_Attribute].[AttrItemVariant] = ''
			and [Set_Attribute].[AttrValue] <> ''
        FOR JSON PATH
		) as [Item.Attributes],
		(
		select [Variant.Code],
			[Variant.Description], 
			[Variant.Description2],
			[Variant.Dim1.DES],
			[Variant.Dim2.DES],
			[Variant.Dim2LOrder],
			[Variant.CommonItemNo],
			[Variant.UnitPriceIncludingVAT],
			[Variant.PromoType],
			[Variant.PriceInclVATB4Promo],
			[Variant.SeasonCode],
			[Variant.AllocKey],
			[Variant.EAN],
			[Variant.Attributes],
			[Variant.Pictures]
		from (
			SELECT VarCode as [Variant.Code],
				(select case when ITLangCode in ('DES', '') then [Set_Variant].[VarDescription] end as 'DES' for json path) as [Variant.Description],
				(select case when ITLangCode in ('DES', '') then [Set_Variant].[VarDescription2] end as 'DES' for json path) as [Variant.Description2],

				[Set_Variant].VarDim1 as [Variant.Dim1.DES],
				[Set_Variant].VarDim2 as [Variant.Dim2.DES],
				[Set_Variant].VarDim2LogicalOrder as [Variant.Dim2LOrder],
				[Set_Variant].VarCommonItemNo as [Variant.CommonItemNo],
				FORMAT([Set_Variant].VarPriceInclVAT,'N2') as [Variant.UnitPriceIncludingVAT],
				[Set_Variant].VarPromoType as [Variant.PromoType],
				FORMAT([Set_Variant].VarPriceInclVATB4Promo,'N2') as [Variant.PriceInclVATB4Promo],
				[Set_Variant].VarSeasonCode as [Variant.SeasonCode],
				[Set_Variant].VarAllocationKey as [Variant.AllocKey],
				[Set_Variant].VarBarcode as [Variant.EAN],
				(
				Select  distinct
					AttrCode as [Attribute.Code], 
					case when ITLangCode in ('DES', '') then case when AttrCode = 'HIST_ARTIKELGRP_ID' THEN REPLACE(REPLACE(AttrValue,'''',''),' ','') ELSE AttrValue END end as [Attribute.Value.DES],
					case when ITLangCode = 'FRS' then [AttrValue] end as [Attribute.Value.FRS]
				from  (select [No],[AttrVariant],AttrCode,AttrValue,ITLangCode
					from dbo.[Item_SET] [Set_Var_Attribute] 
					where [Set_Var_Attribute].[No] = [Set_Variant].[No]
					and [Set_Var_Attribute].[AttrVariant] = [Set_Variant].[VarCode]
					and [Set_Var_Attribute].[AttrValue] <> ''
					UNION
					Select [No],VarCode,'ALLG_COLOR1',VarDim1AVD1 + '#' + VarDim1AV1,'DES' from dbo.[Item_SET] [Set_Var_Attribute] 
					where [Set_Var_Attribute].[No] = [Set_Variant].[No]
					and [Set_Var_Attribute].VarCode = [Set_Variant].[VarCode]
					and [Set_Var_Attribute].VarDim1AVD1 <> '' and VarDim1AV1 <> '' 
					UNION
					Select [No],VarCode,'ALLG_COLOR2',VarDim1AVD2 + '#' + VarDim1AV2,'DES' from dbo.[Item_SET] [Set_Var_Attribute] 
					where [Set_Var_Attribute].[No] = [Set_Variant].[No]
					and [Set_Var_Attribute].VarCode = [Set_Variant].[VarCode]
					and [Set_Var_Attribute].VarDim1AVD2 <> '' and VarDim1AV2 <> ''
					UNION
					Select [No],VarCode,'ALLG_COLOR3',VarDim1AVD3 + '#' + VarDim1AV3,'DES' from dbo.[Item_SET] [Set_Var_Attribute] 
					where [Set_Var_Attribute].[No] = [Set_Variant].[No]
					and [Set_Var_Attribute].VarCode = [Set_Variant].[VarCode]
					and [Set_Var_Attribute].VarDim1AVD3 <> '' and VarDim1AV3 <> ''  
				) as VarAttr
				FOR JSON PATH
				) as [Variant.Attributes],
				(
				Select distinct 
					[PicFileName] as [Picture.Filename],
					format([PicImportDatetime],'yyyyMMddHHmmss') as [Picture.ImportDateTime]
				from dbo.[Item_SET] [Set_Pic]
				where [Set_Pic].[No] = [Set_Variant].[No]
					and [PicItemVariant] = [Set_Variant].[VarCode] and PicFileName <> ''
				FOR JSON PATH
				) as [Variant.Pictures]
			from dbo.[Item_SET] as [Set_Variant]
			where [Set_Variant].[No] = [Set].[No]
			) xy
		group by [Variant.Code],
			[Variant.Dim1.DES],
			[Variant.Dim2.DES],
			[Variant.Dim2LOrder],
			[Variant.CommonItemNo],
			[Variant.UnitPriceIncludingVAT],
			[Variant.PromoType],
			[Variant.PriceInclVATB4Promo],
			[Variant.SeasonCode],
			[Variant.AllocKey],
			[Variant.EAN],
			[Variant.Attributes],
			[Variant.Pictures],
            [Variant.Description],
			[Variant.Description2]
		FOR JSON path
		) as [Item.Variants]
	from #Item [Set]
	) xy
group by 
	[Item.No],
	[Item.SalesUnitofMeasure],
	[Item.VendorItemNo],
	[Item.VendorNo],
	[Item.ItemFamilyCode],
	[Item.LastDateModified],
	[Item.PicturesUpdated],
	[Item.HTMLTexts],
	[Item.Assortments],
	[Item.Pictures],
	[Item.Attributes],
	[Item.Variants],
    [Item.Description],
	[Item.Description2],
	[Item.ShopTitle],
    [Item.ItemFamilyDescr]
FOR JSON path
) Items
for json path, root('Result')

--GOTO EndOfFile --DEBUG
    EndOfFile:
