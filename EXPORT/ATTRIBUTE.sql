select isnull(
    (select distinct 
    [Target].[Code] as 'Attribute.Code',
    [Target].[WSDesc] as 'Attribute.Description.DES',
    [Target].[WSDescFRS] as 'Attribute.Description.FRS',
    [Target].WebshopFilterTypeCode as 'Attribute.WSFilterTypeCode',
    [Target].[UOMDesc] as 'Attribute.UnitOfMeasure.DES',
    [Target].[UOMDescFRS] as 'Attribute.UnitOfMeasure.FRS'
from %1.dbo.[Attribute_SET] as [Target] --on [Target_Log].[Code] = [Target].[Code]  
for JSON PATH, ROOT('Attributes')), '') as [Result]  
