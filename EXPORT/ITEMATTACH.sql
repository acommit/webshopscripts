select isnull(
    (select distinct 
    [Target].[ItemNo] as 'ItemAttachment.ItemNo',
    [Target].[VariantCode] as 'ItemAttachment.VariantCode',
    [Target].[AttachedItemNo] as 'ItemAttachment.AttachedItemNo',
    [Target].[AttachedVariantCode] as 'ItemAttachment.AttachedVariantCode',
    [Target].[Reciprocally] as 'ItemAttachment.Reciprocally'
from %1.dbo.[Export Data Log] as [Log] 
inner join %1.dbo.[ItemAttachment_LOG] as [Target_Log] on [Log].[Batch No_] = [Target_Log].[Batch No_] 
inner join %1.dbo.[ItemAttachment_SET] as [Target] on [Target_Log].[ItemNo] = [Target].[ItemNo]  and [Target_Log].[VariantCode] = [Target].[VariantCode]  and [Target_Log].[AttachedItemNo] = [Target].[AttachedItemNo]  and [Target_Log].[AttachedVariantCode] = [Target].[AttachedVariantCode] where [Log].[Packages Code] = 'WS_EXPORT' 
  and [Log].[Set Name] = 'ITEMATTACH' 
  and [WhereCondition] 
 for JSON PATH, ROOT('ItemAttachments')), '') as [Result]  

update Log 
set [Exported] = 1, 
    [Exported at] = GetDate() 
from %1.dbo.[Export Data Log] [Log] 
where [Log].[Packages Code] = 'WS_EXPORT' 
  and [Log].[Set Name] = 'ITEMATTACH' 
  and [WhereCondition] 