select isnull(
    (select distinct 
    [Target].[ItemNo] as 'ItemAttachment.ItemNo',
    [Target].[VariantCode] as 'ItemAttachment.VariantCode',
    [Target].[AttachedItemNo] as 'ItemAttachment.AttachedItemNo',
    [Target].[AttachedVariantCode] as 'ItemAttachment.AttachedVariantCode',
    [Target].[Reciprocally] as 'ItemAttachment.Reciprocally'
from Webshop_PROD.dbo.[Export Data Log] as [Log] 
inner join Webshop_PROD.dbo.[ItemAttachment_LOG] as [Target_Log] on [Log].[Batch No_] = [Target_Log].[Batch No_] --and [Target_Log].[ItemNo] = '33005921'
inner join Webshop_PROD.dbo.[ItemAttachment_SET] as [Target] on [Target_Log].[ItemNo] = [Target].[ItemNo]  and [Target_Log].[VariantCode] = [Target].[VariantCode]  and [Target_Log].[AttachedItemNo] = [Target].[AttachedItemNo]  and [Target_Log].[AttachedVariantCode] = [Target].[AttachedVariantCode] 
where [Log].[Packages Code] = 'WS_EXPORT' 
  and [Log].[Set Name] = 'ITEMATTACH' 
  --and Log.Exported = 1 --[WhereCondition] 
  and [Target].[ItemNo] = '33005921'
 for JSON PATH, ROOT('ItemAttachments')), '') as [Result]

update Log 
set [Exported] = 1, 
    [Exported at] = GetDate() 
from Webshop_PROD.dbo.[Export Data Log] [Log] 
where [Log].[Packages Code] = 'WS_EXPORT' 
  and [Log].[Set Name] = 'ITEMATTACH' 
  and [WhereCondition] 


select * from [ItemAttachment_LOG] where ItemNo = '33005921'
select * from [ItemAttachment_SET] where ItemNo = '33005921'

select Log.* 
from Webshop_PROD.dbo.[Export Data Log] as [Log] 
inner join Webshop_PROD.dbo.[ItemAttachment_LOG] as [Target_Log] on [Log].[Batch No_] = [Target_Log].[Batch No_] and [Target_Log].[ItemNo] = '33005921'
inner join Webshop_PROD.dbo.[ItemAttachment_SET] as [Target] on [Target_Log].[ItemNo] = [Target].[ItemNo]  and [Target_Log].[VariantCode] = [Target].[VariantCode]  and [Target_Log].[AttachedItemNo] = [Target].[AttachedItemNo]  and [Target_Log].[AttachedVariantCode] = [Target].[AttachedVariantCode] 
where [Log].[Packages Code] = 'WS_EXPORT' 
  and [Log].[Set Name] = 'ITEMATTACH' 
  --and Log.Exported = 0 --[WhereCondition] 
