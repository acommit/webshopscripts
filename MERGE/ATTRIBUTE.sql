declare @FullExport tinyint 
declare @Preview tinyint 
set @FullExport = [FullExport] 
set @Preview = [Preview] 
 
declare @Source Table (
[Code] nvarchar(20),[WSDesc] nvarchar(50),[WSDescFRS] nvarchar(250),[Webshop Filter Type Code] nvarchar(20),[UOMDesc] nvarchar(10),[UOMDescFRS] nvarchar(10))
 
  insert into @Source 
  select distinct [Code], [WSDesc], [WSDescFRS], [Webshop Filter Type Code], [UOMDesc], [UOMDescFRS] 
  from (
[SQLScript] 
  ) as [Source] 

 
begin tran 
declare @BatchNo as int 
set @BatchNo = isnull((select max([Batch No_]) + 1 from [Export Data Log]), 1) 
 
insert into [Export Data Log]([Packages Code], [Set Name], [Merge Completed], [Merge Start Date], [Merge End Date], [Batch No_], [Exported], [Exported at], [Export Timestamp]) 
select 'WS_EXPORT', 'ATTRIBUTE', 0, getdate(), NULL, @BatchNo, 0, 0, NULL 
 
if (@Preview = 0) begin 
  commit; 
  begin tran; 
end 
 
--INSERT
if (@FullExport = 0) begin
  insert into [Attribute_LOG]([Code], [Batch No_], [Generated at], [Type]) 
  select [Source].[Code]
    , @BatchNo, GetDate() as [Generated at], 1 as [Type] 
  from @Source as [Source] 
  left outer join  [Attribute_SET] as [Target] on [Source].[Code] collate database_default  = [Target].[Code] collate database_default 
  where [Target].[UOMDescFRS] is null
end else begin
  update [Log] 
  set [Exported] = 1, 
      [Exported at] = GetDate() 
  from [Export Data Log] [Log] 
  where [Log].[Packages Code] = 'WS_EXPORT' and [Log].[Set Name] = 'ATTRIBUTE' and [Batch No_] <> @BatchNo and [WhereCondition] 
 
  insert into [Attribute_LOG]([Code], [Batch No_], [Generated at],[Type]) 
  select [Source].[Code]
    , @BatchNo, GetDate() as [Generated at], 1 as [Type] 
  from @Source as [Source] 
end 
 
if (@FullExport = 0) begin 
--UPDATE
  insert into [Attribute_LOG]([Code], [Batch No_], [Generated at], [Type]) 
  select [Target].[Code]
    , @BatchNo, GetDate() as [Generated at], 3 as [Type] 
  from [Attribute_SET] as [Target] 
  inner join @Source as [Source] on [Source].[Code] collate database_default  = [Target].[Code] collate database_default 
  where [Source].[WSDesc] collate database_default <> [Target].[WSDesc] collate database_default  or [Source].[WSDescFRS] collate database_default <> [Target].[WSDescFRS] collate database_default  or [Source].[Webshop Filter Type Code] collate database_default <> [Target].[WebshopFilterTypeCode] collate database_default  or [Source].[UOMDesc] collate database_default <> [Target].[UOMDesc] collate database_default  or [Source].[UOMDescFRS] collate database_default <> [Target].[UOMDescFRS] collate database_default 
end 
 
delete 
from [Attribute_SET] 
 
insert into [Attribute_SET]([Code], [WSDesc], [WSDescFRS], [WebshopFilterTypeCode], [UOMDesc], [UOMDescFRS]) 
select [Code], [WSDesc], [WSDescFRS], [Webshop Filter Type Code], [UOMDesc], [UOMDescFRS] 
from @Source 
 
update [Export Data Log] 
set [Merge Completed] = 1, 
    [Merge End Date] = getdate() 
where [Batch No_] = @BatchNo 
if (@Preview = 0) 
  commit 
else begin 
  select [Log].* 
  from [Export Data Log] as [Log] 
  inner join [Attribute_LOG] as [Target_Log] on [Log].[Batch No_] = [Target_Log].[Batch No_] 
  where [Log].[Exported] = 0 
     and [Log].[Packages Code] = 'WS_EXPORT' 
     and [Log].[Set Name] = 'ATTRIBUTE' 
 
  rollback 
end 
 
