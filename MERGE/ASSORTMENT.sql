declare @FullExport tinyint 
declare @Preview tinyint 
set @FullExport = [FullExport] 
set @Preview = [Preview] 
 
declare @Source Table (
[Code] nvarchar(20),[Attached to Code] nvarchar(20),[Description] nvarchar(100),[Description 2] nvarchar(100),[CodingText] nvarchar(100),[ECommerceGroups] nvarchar(250),[TranslationLanguageCode] nvarchar(10),[TranslationDescription] nvarchar(250),[TranslationDescription2] nvarchar(250),[PurchaserName] nvarchar(50),[PurchaserEMail] nvarchar(80),[Sorting ID] int)
 
  insert into @Source 
  select distinct [Code], [Attached to Code], [Description], [Description 2], [CodingText], [ECommerceGroups], [TranslationLanguageCode], [TranslationDescription], [TranslationDescription2], [PurchaserName], [PurchaserEMail], [Sorting ID] 
  from (
[SQLScript] 
  ) as [Source] 

 
begin tran 
declare @BatchNo as int 
set @BatchNo = isnull((select max([Batch No_]) + 1 from [Export Data Log]), 1) 
 
insert into [Export Data Log]([Packages Code], [Set Name], [Merge Completed], [Merge Start Date], [Merge End Date], [Batch No_], [Exported], [Exported at], [Export Timestamp]) 
select 'WS_EXPORT', 'ASSORTMENT', 0, getdate(), NULL, @BatchNo, 0, 0, NULL 
 
if (@Preview = 0) begin 
  commit; 
  begin tran; 
end 
 
--INSERT
if (@FullExport = 0) begin
  insert into [ItemAssortment_LOG]([Code], [AttachedtoCode], [TranslationLanguageCode], [Batch No_], [Generated at], [Type]) 
  select [Source].[Code], [Source].[Attached to Code], [Source].[TranslationLanguageCode]
    , @BatchNo, GetDate() as [Generated at], 1 as [Type] 
  from @Source as [Source] 
  left outer join  [ItemAssortment_SET] as [Target] on [Source].[Code] collate database_default  = [Target].[Code] collate database_default  and [Source].[Attached to Code] collate database_default  = [Target].[AttachedtoCode] collate database_default  and [Source].[TranslationLanguageCode] collate database_default  = [Target].[TranslationLanguageCode] collate database_default 
  where [Target].[SortingID] is null
end else begin
  update [Log] 
  set [Exported] = 1, 
      [Exported at] = GetDate() 
  from [Export Data Log] [Log] 
  where [Log].[Packages Code] = 'WS_EXPORT' and [Log].[Set Name] = 'ASSORTMENT' and [Batch No_] <> @BatchNo and [WhereCondition] 
 
  insert into [ItemAssortment_LOG]([Code], [AttachedtoCode], [TranslationLanguageCode], [Batch No_], [Generated at],[Type]) 
  select [Source].[Code], [Source].[Attached to Code], [Source].[TranslationLanguageCode]
    , @BatchNo, GetDate() as [Generated at], 1 as [Type] 
  from @Source as [Source] 
end 
 
if (@FullExport = 0) begin 
--UPDATE
  insert into [ItemAssortment_LOG]([Code], [AttachedtoCode], [TranslationLanguageCode], [Batch No_], [Generated at], [Type]) 
  select [Target].[Code], [Target].[AttachedtoCode], [Target].[TranslationLanguageCode]
    , @BatchNo, GetDate() as [Generated at], 3 as [Type] 
  from [ItemAssortment_SET] as [Target] 
  inner join @Source as [Source] on [Source].[Code] collate database_default  = [Target].[Code] collate database_default  and [Source].[Attached to Code] collate database_default  = [Target].[AttachedtoCode] collate database_default  and [Source].[TranslationLanguageCode] collate database_default  = [Target].[TranslationLanguageCode] collate database_default 
  where [Source].[Description] collate database_default <> [Target].[Description] collate database_default  or [Source].[Description 2] collate database_default <> [Target].[Description2] collate database_default  or [Source].[CodingText] collate database_default <> [Target].[CodingText] collate database_default  or [Source].[ECommerceGroups] collate database_default <> [Target].[ECommerceGroups] collate database_default  or [Source].[TranslationDescription] collate database_default <> [Target].[TranslationDescription] collate database_default  or [Source].[TranslationDescription2] collate database_default <> [Target].[TranslationDescription2] collate database_default  or [Source].[PurchaserName] collate database_default <> [Target].[PurchaserName] collate database_default  or [Source].[PurchaserEMail] collate database_default <> [Target].[PurchaserEMail] collate database_default  or [Source].[Sorting ID] <> [Target].[SortingID] 
end 
 
delete 
from [ItemAssortment_SET] 
 
insert into [ItemAssortment_SET]([Code], [AttachedtoCode], [Description], [Description2], [CodingText], [ECommerceGroups], [TranslationLanguageCode], [TranslationDescription], [TranslationDescription2], [PurchaserName], [PurchaserEMail], [SortingID]) 
select [Code], [Attached to Code], [Description], [Description 2], [CodingText], [ECommerceGroups], [TranslationLanguageCode], [TranslationDescription], [TranslationDescription2], [PurchaserName], [PurchaserEMail], [Sorting ID] 
from @Source 
 
update [Export Data Log] 
set [Merge Completed] = 1, 
    [Merge End Date] = getdate() 
where [Batch No_] = @BatchNo 
if (@Preview = 0) 
  commit 
else begin 
  select [Log].* 
  from [Export Data Log] as [Log] 
  inner join [ItemAssortment_LOG] as [Target_Log] on [Log].[Batch No_] = [Target_Log].[Batch No_] 
  where [Log].[Exported] = 0 
     and [Log].[Packages Code] = 'WS_EXPORT' 
     and [Log].[Set Name] = 'ASSORTMENT' 
 
  rollback 
end 
 
