declare @FullExport tinyint 
declare @Preview tinyint 
set @FullExport = 0
set @Preview = 1
 
declare @Source Table (
[CommonItemNo] nvarchar(20),[ItemDescription] nvarchar(50),[VarDim1] nvarchar(50),[VarDim2] nvarchar(50),[ItemFamilyCode] nvarchar(10),[ALLG_SHOPLABEL] nvarchar(10),[ItemStatusCode] nvarchar(10),[ALLG_GENDER] nvarchar(10),[ALLG_VELOCARD] nvarchar(10),[ItemPromotionType] int,[VarAllocationKey] nvarchar(20),[DIAS_LONGTAIL] nvarchar(1),[ALLG_TEILSHOP] nvarchar(10),[ALLG_VELOVERSAND] nvarchar(10),[ALLG_WARENKORB] nvarchar(10),[ItemCountryOfOrigin] nvarchar(50),[ItemNoVarianteCode] nvarchar(31),[ItemGenProdPostingGroup] nvarchar(10),[ItemBaseUOM] nvarchar(10),[ItemProdGroupCode] nvarchar(10),[ItemVATProdPostingGroup] nvarchar(10),[ALLG_OBJEKT_TYP] nvarchar(10),[VarPriceInclVAT] decimal(38,20),[VarPriceInclVATB4Promo] decimal(38,20),[ItemDateCreated] datetime,[ItemLastDateModified] datetime,[ItemLeadTimeCalculation] varchar(32),[VarDirectUnitCost] decimal(38,20),[CurrencyCode] nvarchar(10),[PackagingUnit] decimal(38,20),[ConversionPurchInvt] decimal(38,20),[Zero Price Valid] tinyint,[Vendor No_] nvarchar(20),[PurchaserCode] nvarchar(10),[PurchUOM] nvarchar(10),[QtyPerPurchUOM] decimal(38,20),[VarDescription] nvarchar(50),[VarDescription2] nvarchar(50),[WSExcl] tinyint,[VarSeasonCode] nvarchar(10),[ItemHTMLText] nvarchar(max),[ItemHTMLTextPurch] nvarchar(max),[VendorItemNo] nvarchar(20),[VendorBarcodeNo] nvarchar(20),[ItemFamilyDesc] nvarchar(30),[BrandDiscount1] decimal(38,20),[BrandDiscount2] decimal(38,20),[BrandDiscount3] decimal(38,20),[BrandDiscount4] decimal(38,20),[Location Setting] int,[Lagerbereich] nvarchar(20),[Mindestbestand] decimal(38,20),[Losgrösse] decimal(38,20),[Ganze Losgrösse] tinyint,[Berechnung mittlerer EP] int, [ALLG_GEWICHT_G ] decimal(38,20))
 
  insert into @Source 
  select distinct [CommonItemNo], [ItemDescription], [VarDim1], [VarDim2], [ItemFamilyCode], [ALLG_SHOPLABEL], [ItemStatusCode], [ALLG_GENDER], [ALLG_VELOCARD], [ItemPromotionType], [VarAllocationKey], [DIAS_LONGTAIL], [ALLG_TEILSHOP], [ALLG_VELOVERSAND], [ALLG_WARENKORB], [ItemCountryOfOrigin], [ItemNoVarianteCode], [ItemGenProdPostingGroup], [ItemBaseUOM], [ItemProdGroupCode], [ItemVATProdPostingGroup], [ALLG_OBJEKT_TYP], [VarPriceInclVAT], [VarPriceInclVATB4Promo], [ItemDateCreated], [ItemLastDateModified], [ItemLeadTimeCalculation], [VarDirectUnitCost], [CurrencyCode], [PackagingUnit], [ConversionPurchInvt], [Zero Price Valid], [Vendor No_], [PurchaserCode], [PurchUOM], [QtyPerPurchUOM], [VarDescription], [VarDescription2], [WSExcl], [VarSeasonCode], [ItemHTMLText],[ItemHTMLTextPurch], [VendorItemNo], [VendorBarcodeNo], [ItemFamilyDesc], [BrandDiscount1], [BrandDiscount2], [BrandDiscount3], [BrandDiscount4], [Location Setting], [Lagerbereich], [Mindestbestand], [Losgrösse], [Ganze Losgrösse], [Berechnung mittlerer EP], [ALLG_GEWICHT_G ]
  from (
SELECT
  isnull(V.[Common Item No_],'') AS CommonItemNo,   
  I.[Description] as ItemDescription,
  isnull(VD1.[Value Description],'') AS VarDim1,  
  isnull(VD2.[Value Description],'') AS VarDim2,
  I.[Item Family Code] as ItemFamilyCode,
  isnull((select substring([Attribute Value],CHARINDEX('#',[Attribute Value]) + 1,10) from VELO_NAV100CHLS_PROD.dbo.[VeloPlus$Attribute Value] (NOLOCK) where I.[No_] = [Link Field 1] and [Attribute Code] = 'ALLG_SHOPLABEL'),'') as ALLG_SHOPLABEL,
  isnull(IVS.[Status Code],'') as ItemStatusCode,
  isnull((select substring([Attribute Value],CHARINDEX('#',[Attribute Value]) + 1,10) from VELO_NAV100CHLS_PROD.dbo.[VeloPlus$Attribute Value] (NOLOCK) where I.[No_] = [Link Field 1] and [Attribute Code] = 'ALLG_GENDER'),'') as ALLG_GENDER,
  isnull((select substring([Attribute Value],CHARINDEX('#',[Attribute Value]) + 1,10) from VELO_NAV100CHLS_PROD.dbo.[VeloPlus$Attribute Value] (NOLOCK) where I.[No_] = [Link Field 1] and [Attribute Code] = 'ALLG_VELOCARD'),'') as ALLG_VELOCARD,
  isnull(SP.[Promotion Type],isnull(SP_Item.[Promotion Type],0)) as ItemPromotionType,
  isnull(V.[Allocation Key],'') AS VarAllocationKey,
  isnull((select substring([Attribute Value],1,1) from VELO_NAV100CHLS_PROD.dbo.[VeloPlus$Attribute Value] (NOLOCK) where I.[No_] = [Link Field 1] and [Attribute Code] = 'DIAS_LONGTAIL'),'') as DIAS_LONGTAIL,
  isnull((select substring([Attribute Value],CHARINDEX('#',[Attribute Value]) + 1,10) from VELO_NAV100CHLS_PROD.dbo.[VeloPlus$Attribute Value] (NOLOCK) where I.[No_] = [Link Field 1] and [Attribute Code] = 'ALLG_TEILSHOP'),'') as ALLG_TEILSHOP,
  isnull((select TOP 1 substring([Attribute Value],CHARINDEX('#',[Attribute Value]) + 1,10) from VELO_NAV100CHLS_PROD.dbo.[VeloPlus$Attribute Value] (NOLOCK) where I.[No_] = [Link Field 1] and [Attribute Code] = 'ALLG_VELOVERSAND'),'') as ALLG_VELOVERSAND,
  isnull((select substring([Attribute Value],CHARINDEX('#',[Attribute Value]) + 1,10) from VELO_NAV100CHLS_PROD.dbo.[VeloPlus$Attribute Value] (NOLOCK) where I.[No_] = [Link Field 1] and [Attribute Code] = 'ALLG_WARENKORB'),'') as ALLG_WARENKORB,
  --isnull((select substring([Attribute Value],CHARINDEX('#',[Attribute Value]) + 1,10) from VELO_NAV100CHLS_PROD.dbo.[VeloPlus$Attribute Value] (NOLOCK) where I.[No_] = [Link Field 1] and [Attribute Code] = 'ALLG_SHOPSPERRE'),'') as ALLG_SHOPSPERRE,
  isnull((select [Name] from VELO_NAV100CHLS_PROD.dbo.[Country_Region] where Code = I.[Country_Region of Origin Code]),'') as ItemCountryOfOrigin,
  isnull(I.[No_]+ '.'+ V.[Code],'') AS ItemNoVarianteCode,  
  I.[Gen_ Prod_ Posting Group] as ItemGenProdPostingGroup,
  I.[Base Unit of Measure] as ItemBaseUOM,
  I.[Product Group Code] as ItemProdGroupCode,
  I.[VAT Prod_ Posting Group] as ItemVATProdPostingGroup,
  isnull((select substring([Attribute Value],CHARINDEX('#',[Attribute Value]) + 1,10) from VELO_NAV100CHLS_PROD.dbo.[VeloPlus$Attribute Value] (NOLOCK) where I.[No_] = [Link Field 1] and [Attribute Code] = 'ALLG_OBJEKT_TYP'),'') as ALLG_OBJEKT_TYP
  ,isnull(SP.[Unit Price Including VAT],isnull(SP_Item.[Unit Price Including VAT],0)) as VarPriceInclVAT
  ,isnull(SP.[Last Unit Price Before Promo],isnull(SP_Item.[Last Unit Price Before Promo],0)) as VarPriceInclVATB4Promo
  ,CAST(NULL as datetime) as ItemDateCreated
  ,CAST(NULL as datetime) as ItemLastDateModified
  ,I.[Lead Time Calculation] as ItemLeadTimeCalculation
  ,isnull(PP.[Direct Unit Cost],isnull(PP_Item.[Direct Unit Cost],0)) as VarDirectUnitCost
  ,isnull(PP.[Currency Code],isnull(PP_Item.[Currency Code],'')) as CurrencyCode
  ,isnull(PP.[Packaging Unit],isnull(PP_Item.[Packaging Unit],0)) as PackagingUnit
  ,isnull(PP.[Conversion Purchase-Inventory],isnull(PP_Item.[Conversion Purchase-Inventory],0)) as ConversionPurchInvt  
  ,I.[Zero Price Valid]
  ,I.[Vendor No_] 
  ,I.[Purchaser Code] as PurchaserCode
  ,I.[Purch_ Unit of Measure] as PurchUOM
  ,isnull((select [Qty_ per Unit of Measure] from VELO_NAV100CHLS_PROD.dbo.[VeloPlus$Item Unit of Measure] as IUM where IUM.[Item No_] = I.No_ AND Code = I.[Purch_ Unit of Measure]),0) as QtyPerPurchUOM
  ,V.Description as VarDescription
  ,V.[Description 2] as VarDescription2
  ,V.[Webshop excluded] as WSExcl
  ,isnull(nullif(IVR.[Season Code], ''), I.[Season Code]) AS VarSeasonCode 
  ,isnull(IVT.[Text],'') as ItemHTMLText
  ,isnull(IVT2.[Text],'') as ItemHTMLTextPurch
  ,isnull(IV.[Vendor Item No_],'') as VendorItemNo
  ,isnull(IV.[Barcode No_],'') as VendorBarcodeNo
  ,isnull((select [Description] from VELO_NAV100CHLS_PROD.dbo.[VeloPlus$Item Family] where Code = I.[Item Family Code]),'') as ItemFamilyDesc
  ,isnull(BC.[Preorder Discount _],0) as BrandDiscount1
  ,isnull(BC.[Reassort Discount _],0) as BrandDiscount2
  ,isnull(BC.[Special Order Discount _],0) as BrandDiscount3
  ,isnull(BC.[Employes Discount _],0) as BrandDiscount4
  ,V.[Location Setting]
  ,V.[Lagerbereich]
  ,V.[Mindestbestand]
  ,V.[Losgrösse]
  ,V.[Ganze Losgrösse]
  ,V.[Berechnung mittlerer EP]
  ,isnull((select top 1 [Numeric Value] from VELO_NAV100CHLS_PROD.dbo.[VeloPlus$Attribute Value] (NOLOCK) where I.[No_] = [Link Field 1] and V.[Code] = [Link Field 2] and [Attribute Code] = 'ALLG_GEWICHT_G'),0) as [ALLG_GEWICHT_G]
  FROM  VELO_NAV100CHLS_PROD.dbo.[VeloPlus$Item] AS I (NOLOCK)
  INNER JOIN VELO_NAV100CHLS_PROD.dbo.[VeloPlus$Item Variant] AS V (NOLOCK) ON I.[No_] = V.[Item No_]
  LEFT JOIN VELO_NAV100CHLS_PROD.dbo.[VeloPlus$Item Variant Text] AS IVT (NOLOCK) ON IVT.[ItemNo] = I.[No_] and IVT.VarCode in ('',V.Code) and EXISTS (Select * FROM VELO_NAV100CHLS_PROD.dbo.[VeloPlus$Extended Text Header] ETH (NOLOCK) where ETH.[Text No_] = IVT.TextNo and ETH.No_ = IVT.[ItemNo] and ETH.Description = 'Marketingtext')  
  LEFT JOIN VELO_NAV100CHLS_PROD.dbo.[VeloPlus$Item Variant Text] AS IVT2 (NOLOCK)  ON IVT2.[ItemNo] = I.[No_] and IVT2.VarCode in ('',V.Code) and EXISTS (Select * FROM VELO_NAV100CHLS_PROD.dbo.[VeloPlus$Extended Text Header] ETH (NOLOCK) where ETH.[Text No_] = IVT2.TextNo and ETH.No_ = IVT2.[ItemNo] and ETH.Description = 'Einkauf')
  
  LEFT JOIN VELO_NAV100CHLS_PROD.dbo.[VeloPlus$Item Variant Registration] AS IVR (NOLOCK) ON I.[No_] = IVR.[Item No_] and IVR.Variant = V.Code 
  OUTER APPLY (select TOP 1 [Status Code] from VELO_NAV100CHLS_PROD.dbo.[VeloPlus$Item Status Link] (NOLOCK) where I.[No_] = [Item No_] and [Variant Code] = V.Code
               AND [Starting Date] <= GETDATE() Order by [Starting Date] Desc
			) AS IVS 
  OUTER APPLY (SELECT TOP 1 [Unit Price Including VAT], [Promotion Type], [Last Unit Price Before Promo] 
             FROM VELO_NAV100CHLS_PROD.dbo.[VeloPlus$Sales Price] (NOLOCK) 
			 WHERE [Item No_] = I.No_ and [Sales Code] = 'ALL' AND [Variant Code] = V.Code AND [Starting Date] <= GETDATE() AND ([Ending Date] > GETDATE()-1 Or [Ending Date] = '1753-01-01 00:00:00.000')
			 ORDER BY [Starting Date] desc) AS SP 
  OUTER APPLY (SELECT TOP 1 [Unit Price Including VAT], [Promotion Type], [Last Unit Price Before Promo] 
             FROM VELO_NAV100CHLS_PROD.dbo.[VeloPlus$Sales Price] (NOLOCK) 
			 WHERE [Item No_] = I.No_ and [Sales Code] = 'ALL' AND [Variant Code] = '' AND [Starting Date] <= GETDATE() AND ([Ending Date] > GETDATE()-1 Or [Ending Date] = '1753-01-01 00:00:00.000')
			 ORDER BY [Starting Date] desc) AS SP_Item
  OUTER APPLY (select [Vendor Item No_],[Barcode No_] from VELO_NAV100CHLS_PROD.dbo.[VeloPlus$Item Vendor] IV where [Vendor No_] = I.[Vendor No_] and IV.[Item No_] = I.No_ and IV.[Variant Code] = V.Code) AS IV
  OUTER APPLY (SELECT TOP 1 [Direct Unit Cost],[Currency Code],[Packaging Unit],[Conversion Purchase-Inventory]
             FROM VELO_NAV100CHLS_PROD.dbo.[VeloPlus$Purchase Price] (NOLOCK) 
			 WHERE [Item No_] = I.No_ and [Variant Code] = V.Code AND [Starting Date] <= GETDATE() AND ([Ending Date] > GETDATE()-1 Or [Ending Date] = '1753-01-01 00:00:00.000')
			 ORDER BY [Starting Date] desc) AS PP 
  OUTER APPLY (SELECT TOP 1 [Direct Unit Cost],[Currency Code],[Packaging Unit],[Conversion Purchase-Inventory]
             FROM VELO_NAV100CHLS_PROD.dbo.[VeloPlus$Purchase Price] (NOLOCK) 
			 WHERE [Item No_] = I.No_ and [Variant Code] = '' AND [Starting Date] <= GETDATE() AND ([Ending Date] > GETDATE()-1 Or [Ending Date] = '1753-01-01 00:00:00.000')
			 ORDER BY [Starting Date] desc) AS PP_Item
  OUTER APPLY (SELECT TOP 1 [Preorder Discount _],[Reassort Discount _],[Special Order Discount _],[Employes Discount _]
             FROM VELO_NAV100CHLS_PROD.dbo.[VeloPlus$Brand Condition] BC (NOLOCK) 
			 WHERE BC.[Vendor No_] = I.[Vendor No_] and BC.[Item Family Code] = I.[Item Family Code] AND [Valid from Date] <= GETDATE() AND ([Valid until Date] > GETDATE()-1 Or [Valid until Date] = '1753-01-01 00:00:00.000')
			) AS BC 
  OUTER APPLY (SELECT TOP 1 [Value Description] From VELO_NAV100CHLS_PROD.dbo.[VeloPlus$Extended Variant Values] (NOLOCK)
             WHERE [Item No_] = IVR.[Item No_] and [Value] = IVR.[Variant Dimension 1] and Dimension = 1) AS VD1
  OUTER APPLY (SELECT TOP 1 [Value Description] From VELO_NAV100CHLS_PROD.dbo.[VeloPlus$Extended Variant Values] (NOLOCK)
             WHERE [Item No_] = IVR.[Item No_] and [Value] = IVR.[Variant Dimension 2] and Dimension = 2) AS VD2

  ) as [Source] 

 
begin tran 
declare @BatchNo as int 
set @BatchNo = isnull((select max([Batch No_]) + 1 from [Export Data Log]), 1) 
 
insert into [Export Data Log]([Packages Code], [Set Name], [Merge Completed], [Merge Start Date], [Merge End Date], [Batch No_], [Exported], [Exported at], [Export Timestamp]) 
select 'WS_EXPORT', 'ITEMDIAS', 0, getdate(), NULL, @BatchNo, 0, 0, NULL 
 
if (@Preview = 0) begin 
  commit; 
  begin tran; 
end 
 
--INSERT
if (@FullExport = 0) begin
  --insert into [ItemForDias_LOG]([VarDim1], [VarDim2], [VarAllocationKey], [ItemNoVarianteCode], [Batch No_], [Generated at], [Type]) 
  select [Source].[VarDim1], [Source].[VarDim2], [Source].[VarAllocationKey], [Source].[ItemNoVarianteCode]
    , @BatchNo, GetDate() as [Generated at], 1 as [Type] 
  from @Source as [Source] 
  left outer join  [ItemForDias_SET] as [Target] on [Source].[VarDim1] collate database_default  = [Target].[VarDim1] collate database_default  and [Source].[VarDim2] collate database_default  = [Target].[VarDim2] collate database_default  and [Source].[VarAllocationKey] collate database_default  = [Target].[VarAllocationKey] collate database_default  and [Source].[ItemNoVarianteCode] collate database_default  = [Target].[ItemNoVarianteCode] collate database_default 
  where [Target].[BerechnungmittlererEP] is null
end else begin
  update [Log] 
  set [Exported] = 1, 
      [Exported at] = GetDate() 
  from [Export Data Log] [Log] 
  where [Log].[Packages Code] = 'WS_EXPORT' and [Log].[Set Name] = 'ITEMDIAS' and [Batch No_] <> @BatchNo and Log.Exported = 0
 
  --insert into [ItemForDias_LOG]([VarDim1], [VarDim2], [VarAllocationKey], [ItemNoVarianteCode], [Batch No_], [Generated at],[Type]) 
  select [Source].[VarDim1], [Source].[VarDim2], [Source].[VarAllocationKey], [Source].[ItemNoVarianteCode]
    , @BatchNo, GetDate() as [Generated at], 1 as [Type] 
  from @Source as [Source] 
end 
 
if (@FullExport = 0) begin 
--UPDATE
  --insert into [ItemForDias_LOG]([VarDim1], [VarDim2], [VarAllocationKey], [ItemNoVarianteCode], [Batch No_], [Generated at], [Type]) 
  select [Target].[VarDim1], [Target].[VarDim2], [Target].[VarAllocationKey], [Target].[ItemNoVarianteCode]
    , @BatchNo, GetDate() as [Generated at], 3 as [Type] 
  from [ItemForDias_SET] as [Target] 
  inner join @Source as [Source] on [Source].[VarDim1] collate database_default  = [Target].[VarDim1] collate database_default  and [Source].[VarDim2] collate database_default  = [Target].[VarDim2] collate database_default  and [Source].[VarAllocationKey] collate database_default  = [Target].[VarAllocationKey] collate database_default  and [Source].[ItemNoVarianteCode] collate database_default  = [Target].[ItemNoVarianteCode] collate database_default 
  where [Source].[CommonItemNo] collate database_default <> [Target].[CommonItemNo] collate database_default  or [Source].[ItemDescription] collate database_default <> [Target].[ItemDescription] collate database_default  or [Source].[ItemFamilyCode] collate database_default <> [Target].[ItemFamilyCode] collate database_default  or [Source].[ALLG_SHOPLABEL] collate database_default <> [Target].[ALLGSHOPLABEL] collate database_default  or [Source].[ItemStatusCode] collate database_default <> [Target].[ItemStatusCode] collate database_default  or [Source].[ALLG_GENDER] collate database_default <> [Target].[ALLGGENDER] collate database_default  or [Source].[ALLG_VELOCARD] collate database_default <> [Target].[ALLGVELOCARD] collate database_default  or [Source].[ItemPromotionType] <> [Target].[ItemPromotionType]  or [Source].[DIAS_LONGTAIL] collate database_default <> [Target].[DIASLONGTAIL] collate database_default  or [Source].[ALLG_TEILSHOP] collate database_default <> [Target].[ALLGTEILSHOP] collate database_default  or [Source].[ALLG_VELOVERSAND] collate database_default <> [Target].[ALLGVELOVERSAND] collate database_default  or [Source].[ALLG_WARENKORB] collate database_default <> [Target].[ALLGWARENKORB] collate database_default  or [Source].[ItemCountryOfOrigin] collate database_default <> [Target].[ItemCountryOfOrigin] collate database_default  or [Source].[ItemGenProdPostingGroup] collate database_default <> [Target].[ItemGenProdPostingGroup] collate database_default  or [Source].[ItemBaseUOM] collate database_default <> [Target].[ItemBaseUOM] collate database_default  or [Source].[ItemProdGroupCode] collate database_default <> [Target].[ItemProdGroupCode] collate database_default  or [Source].[ItemVATProdPostingGroup] collate database_default <> [Target].[ItemVATProdPostingGroup] collate database_default  or [Source].[ALLG_OBJEKT_TYP] collate database_default <> [Target].[ALLGOBJEKTTYP] collate database_default  or [Source].[VarPriceInclVAT] <> [Target].[VarPriceInclVAT]  or [Source].[VarPriceInclVATB4Promo] <> [Target].[VarPriceInclVATB4Promo]  or [Source].[ItemDateCreated] <> [Target].[ItemDateCreated]  or [Source].[ItemLastDateModified] <> [Target].[ItemLastDateModified]  or [Source].[ItemLeadTimeCalculation] collate database_default <> [Target].[ItemLeadTimeCalculation] collate database_default  or [Source].[VarDirectUnitCost] <> [Target].[VarDirectUnitCost]  or [Source].[CurrencyCode] collate database_default <> [Target].[CurrencyCode] collate database_default  or [Source].[PackagingUnit] <> [Target].[PackagingUnit]  or [Source].[ConversionPurchInvt] <> [Target].[ConversionPurchInvt]  or [Source].[Zero Price Valid] <> [Target].[ZeroPriceValid]  or [Source].[Vendor No_] collate database_default <> [Target].[VendorNo] collate database_default  or [Source].[PurchaserCode] collate database_default <> [Target].[PurchaserCode] collate database_default  or [Source].[PurchUOM] collate database_default <> [Target].[PurchUOM] collate database_default  or [Source].[QtyPerPurchUOM] <> [Target].[QtyPerPurchUOM]  or [Source].[VarDescription] collate database_default <> [Target].[VarDescription] collate database_default  or [Source].[VarDescription2] collate database_default <> [Target].[VarDescription2] collate database_default  or [Source].[WSExcl] <> [Target].[WSExcl]  or [Source].[VarSeasonCode] collate database_default <> [Target].[VarSeasonCode] collate database_default  or [Source].[ItemHTMLText] collate database_default <> [Target].[ItemHTMLText] collate database_default  or [Source].[ItemHTMLTextPurch] collate database_default <> [Target].[ItemHTMLTextPurch] collate database_default or [Source].[VendorItemNo] collate database_default <> [Target].[VendorItemNo] collate database_default  or [Source].[VendorBarcodeNo] collate database_default <> [Target].[VendorBarcodeNo] collate database_default  or [Source].[ItemFamilyDesc] collate database_default <> [Target].[ItemFamilyDesc] collate database_default  or [Source].[BrandDiscount1] <> [Target].[BrandDiscount1]  or [Source].[BrandDiscount2] <> [Target].[BrandDiscount2]  or [Source].[BrandDiscount3] <> [Target].[BrandDiscount3]  or [Source].[BrandDiscount4] <> [Target].[BrandDiscount4]  or [Source].[Location Setting] <> [Target].[LocationSetting]  or [Source].[Lagerbereich] collate database_default <> [Target].[Lagerbereich] collate database_default  or [Source].[Mindestbestand] <> [Target].[Mindestbestand]  or [Source].[Losgrösse] <> [Target].[Losgrsse]  or [Source].[Ganze Losgrösse] <> [Target].[GanzeLosgrsse]  or [Source].[Berechnung mittlerer EP] <> [Target].[BerechnungmittlererEP] or [Source].[ALLG_GEWICHT_G] <> [Target].[ALLGGEWICHTG]
end 
 
--delete 
--from [ItemForDias_SET] 
 
--insert into [ItemForDias_SET]([CommonItemNo], [ItemDescription], [VarDim1], [VarDim2], [ItemFamilyCode], [ALLGSHOPLABEL], [ItemStatusCode], [ALLGGENDER], [ALLGVELOCARD], [ItemPromotionType], [VarAllocationKey], [DIASLONGTAIL], [ALLGTEILSHOP], [ALLGVELOVERSAND], [ALLGWARENKORB], [ItemCountryOfOrigin], [ItemNoVarianteCode], [ItemGenProdPostingGroup], [ItemBaseUOM], [ItemProdGroupCode], [ItemVATProdPostingGroup], [ALLGOBJEKTTYP], [VarPriceInclVAT], [VarPriceInclVATB4Promo], [ItemDateCreated], [ItemLastDateModified], [ItemLeadTimeCalculation], [VarDirectUnitCost], [CurrencyCode], [PackagingUnit], [ConversionPurchInvt], [ZeroPriceValid], [VendorNo], [PurchaserCode], [PurchUOM], [QtyPerPurchUOM], [VarDescription], [VarDescription2], [WSExcl], [VarSeasonCode], [ItemHTMLText], [ItemHTMLTextPurch], [VendorItemNo], [VendorBarcodeNo], [ItemFamilyDesc], [BrandDiscount1], [BrandDiscount2], [BrandDiscount3], [BrandDiscount4], [LocationSetting], [Lagerbereich], [Mindestbestand], [Losgrsse], [GanzeLosgrsse], [BerechnungmittlererEP], [ALLGGEWICHTG]) 
select [CommonItemNo], [ItemDescription], [VarDim1], [VarDim2], [ItemFamilyCode], [ALLG_SHOPLABEL], [ItemStatusCode], [ALLG_GENDER], [ALLG_VELOCARD], [ItemPromotionType], [VarAllocationKey], [DIAS_LONGTAIL], [ALLG_TEILSHOP], [ALLG_VELOVERSAND], [ALLG_WARENKORB], [ItemCountryOfOrigin], [ItemNoVarianteCode], [ItemGenProdPostingGroup], [ItemBaseUOM], [ItemProdGroupCode], [ItemVATProdPostingGroup], [ALLG_OBJEKT_TYP], [VarPriceInclVAT], [VarPriceInclVATB4Promo], [ItemDateCreated], [ItemLastDateModified], [ItemLeadTimeCalculation], [VarDirectUnitCost], [CurrencyCode], [PackagingUnit], [ConversionPurchInvt], [Zero Price Valid], [Vendor No_], [PurchaserCode], [PurchUOM], [QtyPerPurchUOM], [VarDescription], [VarDescription2], [WSExcl], [VarSeasonCode], [ItemHTMLText], [ItemHTMLTextPurch], [VendorItemNo], [VendorBarcodeNo], [ItemFamilyDesc], [BrandDiscount1], [BrandDiscount2], [BrandDiscount3], [BrandDiscount4], [Location Setting], [Lagerbereich], [Mindestbestand], [Losgrösse], [Ganze Losgrösse], [Berechnung mittlerer EP], [ALLG_GEWICHT_G] 
from @Source 
 
update [Export Data Log] 
set [Merge Completed] = 1, 
    [Merge End Date] = getdate() 
where [Batch No_] = @BatchNo 
if (@Preview = 0) 
  commit 
else begin 
  select [Log].* 
  from [Export Data Log] as [Log] 
  inner join [ItemForDias_LOG] as [Target_Log] on [Log].[Batch No_] = [Target_Log].[Batch No_] 
  where [Log].[Exported] = 0 
     and [Log].[Packages Code] = 'WS_EXPORT' 
     and [Log].[Set Name] = 'ITEMDIAS' 
 
  rollback 
end 
 
select * from ItemForDias