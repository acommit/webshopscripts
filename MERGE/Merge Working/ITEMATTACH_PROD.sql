
Use Webshop_PROD
go
declare @FullExport tinyint 
declare @Preview tinyint 
--set @FullExport = [FullExport] 
--set @Preview = [Preview] 

set @FullExport = 0
set @Preview = 1
 
declare @Source Table (
[Item No_] nvarchar(20),[Variant Code] nvarchar(10),[Attached Item No_] nvarchar(20),[Attached Variant Code] nvarchar(10),[Reciprocally] tinyint)
 
  insert into @Source 
  select distinct [Item No_], [Variant Code], [Attached Item No_], [Attached Variant Code], [Reciprocally] 
  from (
SELECT 
[Item No_],
[Variant Code],
[Attached Item No_],
[Attached Variant Code],
[Reciprocally]
 FROM VELO_NAV100CHLS_PROD.dbo.[VeloPlus$Item Attachments]
  ) as [Source]
 
if (@FullExport = 0) begin
  --insert into [ItemAttachment_LOG]([ItemNo], [VariantCode], [AttachedItemNo], [AttachedVariantCode], [Batch No_], [Generated at], [Type]) 
  select [Source].[Item No_], [Source].[Variant Code], [Source].[Attached Item No_], [Source].[Attached Variant Code]
    --, @BatchNo, GetDate() as [Generated at], 1 as [Type] 
  from @Source as [Source] 
  left outer join  [ItemAttachment_SET] as [Target] on [Source].[Item No_] collate database_default  = [Target].[ItemNo] collate database_default  and [Source].[Variant Code] collate database_default  = [Target].[VariantCode] collate database_default  and [Source].[Attached Item No_] collate database_default  = [Target].[AttachedItemNo] collate database_default  and [Source].[Attached Variant Code] collate database_default  = [Target].[AttachedVariantCode] collate database_default 
  where [Target].[Reciprocally] is null
end else begin
--   update [Log] 
--   set [Exported] = 1, 
--       [Exported at] = GetDate() 
--   from [Export Data Log] [Log] 
--   where [Log].[Packages Code] = 'WS_EXPORT' and [Log].[Set Name] = 'ITEMATTACH' and [Batch No_] <> @BatchNo and Log.Exported = 0 
 
  --insert into [ItemAttachment_LOG]([ItemNo], [VariantCode], [AttachedItemNo], [AttachedVariantCode], [Batch No_], [Generated at],[Type]) 
  select [Source].[Item No_], [Source].[Variant Code], [Source].[Attached Item No_], [Source].[Attached Variant Code]
    --, @BatchNo, GetDate() as [Generated at], 1 as [Type] 
  from @Source as [Source] 
end 
 
if (@FullExport = 0) begin 
--UPDATE
  --insert into [ItemAttachment_LOG]([ItemNo], [VariantCode], [AttachedItemNo], [AttachedVariantCode], [Batch No_], [Generated at], [Type]) 
  select [Target].[ItemNo], [Target].[VariantCode], [Target].[AttachedItemNo], [Target].[AttachedVariantCode]
    --, @BatchNo, GetDate() as [Generated at], 3 as [Type] 
  from [ItemAttachment_SET] as [Target] 
  inner join @Source as [Source] on [Source].[Item No_] collate database_default  = [Target].[ItemNo] collate database_default  and [Source].[Variant Code] collate database_default  = [Target].[VariantCode] collate database_default  and [Source].[Attached Item No_] collate database_default  = [Target].[AttachedItemNo] collate database_default  and [Source].[Attached Variant Code] collate database_default  = [Target].[AttachedVariantCode] collate database_default 
  where [Source].[Reciprocally] <> [Target].[Reciprocally] 
end 
--DELETE from [ItemAttachment_LOG]
--delete from [ItemAttachment_SET]
select count(*) from [ItemAttachment_LOG] where ItemNo = '33005921'
select count(*) from [ItemAttachment_SET]