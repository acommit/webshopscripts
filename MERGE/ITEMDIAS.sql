declare @FullExport tinyint 
declare @Preview tinyint 
set @FullExport = [FullExport] 
set @Preview = [Preview] 
 
declare @Source Table (
[CommonItemNo] nvarchar(20),[ItemDescription] nvarchar(50),[VarDim1] nvarchar(50),[VarDim2] nvarchar(50),[ItemFamilyCode] nvarchar(10),[ALLG_SHOPLABEL] nvarchar(10),[ItemStatusCode] nvarchar(10),[ALLG_GENDER] nvarchar(10),[ALLG_VELOCARD] nvarchar(10),[ItemPromotionType] int,[VarAllocationKey] nvarchar(20),[DIAS_LONGTAIL] nvarchar(1),[ALLG_TEILSHOP] nvarchar(10),[ALLG_VELOVERSAND] nvarchar(10),[ALLG_WARENKORB] nvarchar(10),[ItemCountryOfOrigin] nvarchar(50),[ItemNoVarianteCode] nvarchar(31),[ItemGenProdPostingGroup] nvarchar(10),[ItemBaseUOM] nvarchar(10),[ItemProdGroupCode] nvarchar(10),[ItemVATProdPostingGroup] nvarchar(10),[ALLG_OBJEKT_TYP] nvarchar(10),[VarPriceInclVAT] decimal(38,20),[VarPriceInclVATB4Promo] decimal(38,20),[ItemDateCreated] datetime,[ItemLastDateModified] datetime,[ItemLeadTimeCalculation] varchar(32),[VarDirectUnitCost] decimal(38,20),[CurrencyCode] nvarchar(10),[PackagingUnit] decimal(38,20),[ConversionPurchInvt] decimal(38,20),[Zero Price Valid] tinyint,[Vendor No_] nvarchar(20),[PurchaserCode] nvarchar(10),[PurchUOM] nvarchar(10),[QtyPerPurchUOM] decimal(38,20),[VarDescription] nvarchar(50),[VarDescription2] nvarchar(50),[WSExcl] tinyint,[VarSeasonCode] nvarchar(10),[ItemHTMLText] nvarchar(max),[ItemHTMLTextPurch] nvarchar(max),[VendorItemNo] nvarchar(20),[VendorBarcodeNo] nvarchar(20),[ItemFamilyDesc] nvarchar(30),[BrandDiscount1] decimal(38,20),[BrandDiscount2] decimal(38,20),[BrandDiscount3] decimal(38,20),[BrandDiscount4] decimal(38,20),[Location Setting] int,[Lagerbereich] nvarchar(20),[Mindestbestand] decimal(38,20),[Losgrösse] decimal(38,20),[Ganze Losgrösse] tinyint,[Berechnung mittlerer EP] int, [ALLG_GEWICHT_G ] decimal(38,20))
 
  insert into @Source 
  select distinct [CommonItemNo], [ItemDescription], [VarDim1], [VarDim2], [ItemFamilyCode], [ALLG_SHOPLABEL], [ItemStatusCode], [ALLG_GENDER], [ALLG_VELOCARD], [ItemPromotionType], [VarAllocationKey], [DIAS_LONGTAIL], [ALLG_TEILSHOP], [ALLG_VELOVERSAND], [ALLG_WARENKORB], [ItemCountryOfOrigin], [ItemNoVarianteCode], [ItemGenProdPostingGroup], [ItemBaseUOM], [ItemProdGroupCode], [ItemVATProdPostingGroup], [ALLG_OBJEKT_TYP], [VarPriceInclVAT], [VarPriceInclVATB4Promo], [ItemDateCreated], [ItemLastDateModified], [ItemLeadTimeCalculation], [VarDirectUnitCost], [CurrencyCode], [PackagingUnit], [ConversionPurchInvt], [Zero Price Valid], [Vendor No_], [PurchaserCode], [PurchUOM], [QtyPerPurchUOM], [VarDescription], [VarDescription2], [WSExcl], [VarSeasonCode], [ItemHTMLText],[ItemHTMLTextPurch], [VendorItemNo], [VendorBarcodeNo], [ItemFamilyDesc], [BrandDiscount1], [BrandDiscount2], [BrandDiscount3], [BrandDiscount4], [Location Setting], [Lagerbereich], [Mindestbestand], [Losgrösse], [Ganze Losgrösse], [Berechnung mittlerer EP], [ALLG_GEWICHT_G ]
  from (
[SQLScript] 
  ) as [Source] 

 
begin tran 
declare @BatchNo as int 
set @BatchNo = isnull((select max([Batch No_]) + 1 from [Export Data Log]), 1) 
 
insert into [Export Data Log]([Packages Code], [Set Name], [Merge Completed], [Merge Start Date], [Merge End Date], [Batch No_], [Exported], [Exported at], [Export Timestamp]) 
select 'WS_EXPORT', 'ITEMDIAS', 0, getdate(), NULL, @BatchNo, 0, 0, NULL 
 
if (@Preview = 0) begin 
  commit; 
  begin tran; 
end 
 
--INSERT
if (@FullExport = 0) begin
  insert into [ItemForDias_LOG]([VarDim1], [VarDim2], [VarAllocationKey], [ItemNoVarianteCode], [Batch No_], [Generated at], [Type]) 
  select [Source].[VarDim1], [Source].[VarDim2], [Source].[VarAllocationKey], [Source].[ItemNoVarianteCode]
    , @BatchNo, GetDate() as [Generated at], 1 as [Type] 
  from @Source as [Source] 
  left outer join  [ItemForDias_SET] as [Target] on [Source].[VarDim1] collate database_default  = [Target].[VarDim1] collate database_default  and [Source].[VarDim2] collate database_default  = [Target].[VarDim2] collate database_default  and [Source].[VarAllocationKey] collate database_default  = [Target].[VarAllocationKey] collate database_default  and [Source].[ItemNoVarianteCode] collate database_default  = [Target].[ItemNoVarianteCode] collate database_default 
  where [Target].[BerechnungmittlererEP] is null
end else begin
  update [Log] 
  set [Exported] = 1, 
      [Exported at] = GetDate() 
  from [Export Data Log] [Log] 
  where [Log].[Packages Code] = 'WS_EXPORT' and [Log].[Set Name] = 'ITEMDIAS' and [Batch No_] <> @BatchNo and [WhereCondition] 
 
  insert into [ItemForDias_LOG]([VarDim1], [VarDim2], [VarAllocationKey], [ItemNoVarianteCode], [Batch No_], [Generated at],[Type]) 
  select [Source].[VarDim1], [Source].[VarDim2], [Source].[VarAllocationKey], [Source].[ItemNoVarianteCode]
    , @BatchNo, GetDate() as [Generated at], 1 as [Type] 
  from @Source as [Source] 
end 
 
if (@FullExport = 0) begin 
--UPDATE
  insert into [ItemForDias_LOG]([VarDim1], [VarDim2], [VarAllocationKey], [ItemNoVarianteCode], [Batch No_], [Generated at], [Type]) 
  select [Target].[VarDim1], [Target].[VarDim2], [Target].[VarAllocationKey], [Target].[ItemNoVarianteCode]
    , @BatchNo, GetDate() as [Generated at], 3 as [Type] 
  from [ItemForDias_SET] as [Target] 
  inner join @Source as [Source] on [Source].[VarDim1] collate database_default  = [Target].[VarDim1] collate database_default  and [Source].[VarDim2] collate database_default  = [Target].[VarDim2] collate database_default  and [Source].[VarAllocationKey] collate database_default  = [Target].[VarAllocationKey] collate database_default  and [Source].[ItemNoVarianteCode] collate database_default  = [Target].[ItemNoVarianteCode] collate database_default 
  where [Source].[CommonItemNo] collate database_default <> [Target].[CommonItemNo] collate database_default  or [Source].[ItemDescription] collate database_default <> [Target].[ItemDescription] collate database_default  or [Source].[ItemFamilyCode] collate database_default <> [Target].[ItemFamilyCode] collate database_default  or [Source].[ALLG_SHOPLABEL] collate database_default <> [Target].[ALLGSHOPLABEL] collate database_default  or [Source].[ItemStatusCode] collate database_default <> [Target].[ItemStatusCode] collate database_default  or [Source].[ALLG_GENDER] collate database_default <> [Target].[ALLGGENDER] collate database_default  or [Source].[ALLG_VELOCARD] collate database_default <> [Target].[ALLGVELOCARD] collate database_default  or [Source].[ItemPromotionType] <> [Target].[ItemPromotionType]  or [Source].[DIAS_LONGTAIL] collate database_default <> [Target].[DIASLONGTAIL] collate database_default  or [Source].[ALLG_TEILSHOP] collate database_default <> [Target].[ALLGTEILSHOP] collate database_default  or [Source].[ALLG_VELOVERSAND] collate database_default <> [Target].[ALLGVELOVERSAND] collate database_default  or [Source].[ALLG_WARENKORB] collate database_default <> [Target].[ALLGWARENKORB] collate database_default  or [Source].[ItemCountryOfOrigin] collate database_default <> [Target].[ItemCountryOfOrigin] collate database_default  or [Source].[ItemGenProdPostingGroup] collate database_default <> [Target].[ItemGenProdPostingGroup] collate database_default  or [Source].[ItemBaseUOM] collate database_default <> [Target].[ItemBaseUOM] collate database_default  or [Source].[ItemProdGroupCode] collate database_default <> [Target].[ItemProdGroupCode] collate database_default  or [Source].[ItemVATProdPostingGroup] collate database_default <> [Target].[ItemVATProdPostingGroup] collate database_default  or [Source].[ALLG_OBJEKT_TYP] collate database_default <> [Target].[ALLGOBJEKTTYP] collate database_default  or [Source].[VarPriceInclVAT] <> [Target].[VarPriceInclVAT]  or [Source].[VarPriceInclVATB4Promo] <> [Target].[VarPriceInclVATB4Promo]  or [Source].[ItemDateCreated] <> [Target].[ItemDateCreated]  or [Source].[ItemLastDateModified] <> [Target].[ItemLastDateModified]  or [Source].[ItemLeadTimeCalculation] collate database_default <> [Target].[ItemLeadTimeCalculation] collate database_default  or [Source].[VarDirectUnitCost] <> [Target].[VarDirectUnitCost]  or [Source].[CurrencyCode] collate database_default <> [Target].[CurrencyCode] collate database_default  or [Source].[PackagingUnit] <> [Target].[PackagingUnit]  or [Source].[ConversionPurchInvt] <> [Target].[ConversionPurchInvt]  or [Source].[Zero Price Valid] <> [Target].[ZeroPriceValid]  or [Source].[Vendor No_] collate database_default <> [Target].[VendorNo] collate database_default  or [Source].[PurchaserCode] collate database_default <> [Target].[PurchaserCode] collate database_default  or [Source].[PurchUOM] collate database_default <> [Target].[PurchUOM] collate database_default  or [Source].[QtyPerPurchUOM] <> [Target].[QtyPerPurchUOM]  or [Source].[VarDescription] collate database_default <> [Target].[VarDescription] collate database_default  or [Source].[VarDescription2] collate database_default <> [Target].[VarDescription2] collate database_default  or [Source].[WSExcl] <> [Target].[WSExcl]  or [Source].[VarSeasonCode] collate database_default <> [Target].[VarSeasonCode] collate database_default  or [Source].[ItemHTMLText] collate database_default <> [Target].[ItemHTMLText] collate database_default  or [Source].[ItemHTMLTextPurch] collate database_default <> [Target].[ItemHTMLTextPurch] collate database_default or [Source].[VendorItemNo] collate database_default <> [Target].[VendorItemNo] collate database_default  or [Source].[VendorBarcodeNo] collate database_default <> [Target].[VendorBarcodeNo] collate database_default  or [Source].[ItemFamilyDesc] collate database_default <> [Target].[ItemFamilyDesc] collate database_default  or [Source].[BrandDiscount1] <> [Target].[BrandDiscount1]  or [Source].[BrandDiscount2] <> [Target].[BrandDiscount2]  or [Source].[BrandDiscount3] <> [Target].[BrandDiscount3]  or [Source].[BrandDiscount4] <> [Target].[BrandDiscount4]  or [Source].[Location Setting] <> [Target].[LocationSetting]  or [Source].[Lagerbereich] collate database_default <> [Target].[Lagerbereich] collate database_default  or [Source].[Mindestbestand] <> [Target].[Mindestbestand]  or [Source].[Losgrösse] <> [Target].[Losgrsse]  or [Source].[Ganze Losgrösse] <> [Target].[GanzeLosgrsse]  or [Source].[Berechnung mittlerer EP] <> [Target].[BerechnungmittlererEP] or [Source].[ALLG_GEWICHT_G] <> [Target].[ALLGGEWICHTG]
end 
 
delete 
from [ItemForDias_SET] 
 
insert into [ItemForDias_SET]([CommonItemNo], [ItemDescription], [VarDim1], [VarDim2], [ItemFamilyCode], [ALLGSHOPLABEL], [ItemStatusCode], [ALLGGENDER], [ALLGVELOCARD], [ItemPromotionType], [VarAllocationKey], [DIASLONGTAIL], [ALLGTEILSHOP], [ALLGVELOVERSAND], [ALLGWARENKORB], [ItemCountryOfOrigin], [ItemNoVarianteCode], [ItemGenProdPostingGroup], [ItemBaseUOM], [ItemProdGroupCode], [ItemVATProdPostingGroup], [ALLGOBJEKTTYP], [VarPriceInclVAT], [VarPriceInclVATB4Promo], [ItemDateCreated], [ItemLastDateModified], [ItemLeadTimeCalculation], [VarDirectUnitCost], [CurrencyCode], [PackagingUnit], [ConversionPurchInvt], [ZeroPriceValid], [VendorNo], [PurchaserCode], [PurchUOM], [QtyPerPurchUOM], [VarDescription], [VarDescription2], [WSExcl], [VarSeasonCode], [ItemHTMLText], [ItemHTMLTextPurch], [VendorItemNo], [VendorBarcodeNo], [ItemFamilyDesc], [BrandDiscount1], [BrandDiscount2], [BrandDiscount3], [BrandDiscount4], [LocationSetting], [Lagerbereich], [Mindestbestand], [Losgrsse], [GanzeLosgrsse], [BerechnungmittlererEP], [ALLGGEWICHTG]) 
select [CommonItemNo], [ItemDescription], [VarDim1], [VarDim2], [ItemFamilyCode], [ALLG_SHOPLABEL], [ItemStatusCode], [ALLG_GENDER], [ALLG_VELOCARD], [ItemPromotionType], [VarAllocationKey], [DIAS_LONGTAIL], [ALLG_TEILSHOP], [ALLG_VELOVERSAND], [ALLG_WARENKORB], [ItemCountryOfOrigin], [ItemNoVarianteCode], [ItemGenProdPostingGroup], [ItemBaseUOM], [ItemProdGroupCode], [ItemVATProdPostingGroup], [ALLG_OBJEKT_TYP], [VarPriceInclVAT], [VarPriceInclVATB4Promo], [ItemDateCreated], [ItemLastDateModified], [ItemLeadTimeCalculation], [VarDirectUnitCost], [CurrencyCode], [PackagingUnit], [ConversionPurchInvt], [Zero Price Valid], [Vendor No_], [PurchaserCode], [PurchUOM], [QtyPerPurchUOM], [VarDescription], [VarDescription2], [WSExcl], [VarSeasonCode], [ItemHTMLText], [ItemHTMLTextPurch], [VendorItemNo], [VendorBarcodeNo], [ItemFamilyDesc], [BrandDiscount1], [BrandDiscount2], [BrandDiscount3], [BrandDiscount4], [Location Setting], [Lagerbereich], [Mindestbestand], [Losgrösse], [Ganze Losgrösse], [Berechnung mittlerer EP], [ALLG_GEWICHT_G] 
from @Source 
 
update [Export Data Log] 
set [Merge Completed] = 1, 
    [Merge End Date] = getdate() 
where [Batch No_] = @BatchNo 
if (@Preview = 0) 
  commit 
else begin 
  select [Log].* 
  from [Export Data Log] as [Log] 
  inner join [ItemForDias_LOG] as [Target_Log] on [Log].[Batch No_] = [Target_Log].[Batch No_] 
  where [Log].[Exported] = 0 
     and [Log].[Packages Code] = 'WS_EXPORT' 
     and [Log].[Set Name] = 'ITEMDIAS' 
 
  rollback 
end 
 
