declare @FullExport tinyint 
declare @Preview tinyint 
set @FullExport = [FullExport] 
set @Preview = [Preview] 
 
declare @Source Table (
[Item No_] nvarchar(20),[Variant Code] nvarchar(10),[Attached Item No_] nvarchar(20),[Attached Variant Code] nvarchar(10),[Reciprocally] tinyint)
 
  insert into @Source 
  select distinct [Item No_], [Variant Code], [Attached Item No_], [Attached Variant Code], [Reciprocally] 
  from (
[SQLScript] 
  ) as [Source] 

 
begin tran 
declare @BatchNo as int 
set @BatchNo = isnull((select max([Batch No_]) + 1 from [Export Data Log]), 1) 
 
insert into [Export Data Log]([Packages Code], [Set Name], [Merge Completed], [Merge Start Date], [Merge End Date], [Batch No_], [Exported], [Exported at], [Export Timestamp]) 
select 'WS_EXPORT', 'ITEMATTACH', 0, getdate(), NULL, @BatchNo, 0, 0, NULL 
 
if (@Preview = 0) begin 
  commit; 
  begin tran; 
end 
 
--INSERT
if (@FullExport = 0) begin
  insert into [ItemAttachment_LOG]([ItemNo], [VariantCode], [AttachedItemNo], [AttachedVariantCode], [Batch No_], [Generated at], [Type]) 
  select [Source].[Item No_], [Source].[Variant Code], [Source].[Attached Item No_], [Source].[Attached Variant Code]
    , @BatchNo, GetDate() as [Generated at], 1 as [Type] 
  from @Source as [Source] 
  left outer join  [ItemAttachment_SET] as [Target] on [Source].[Item No_] collate database_default  = [Target].[ItemNo] collate database_default  and [Source].[Variant Code] collate database_default  = [Target].[VariantCode] collate database_default  and [Source].[Attached Item No_] collate database_default  = [Target].[AttachedItemNo] collate database_default  and [Source].[Attached Variant Code] collate database_default  = [Target].[AttachedVariantCode] collate database_default 
  where [Target].[Reciprocally] is null
end else begin
  update [Log] 
  set [Exported] = 1, 
      [Exported at] = GetDate() 
  from [Export Data Log] [Log] 
  where [Log].[Packages Code] = 'WS_EXPORT' and [Log].[Set Name] = 'ITEMATTACH' and [Batch No_] <> @BatchNo and [WhereCondition] 
 
  insert into [ItemAttachment_LOG]([ItemNo], [VariantCode], [AttachedItemNo], [AttachedVariantCode], [Batch No_], [Generated at],[Type]) 
  select [Source].[Item No_], [Source].[Variant Code], [Source].[Attached Item No_], [Source].[Attached Variant Code]
    , @BatchNo, GetDate() as [Generated at], 1 as [Type] 
  from @Source as [Source] 
end 
 
if (@FullExport = 0) begin 
--UPDATE
  insert into [ItemAttachment_LOG]([ItemNo], [VariantCode], [AttachedItemNo], [AttachedVariantCode], [Batch No_], [Generated at], [Type]) 
  select [Target].[ItemNo], [Target].[VariantCode], [Target].[AttachedItemNo], [Target].[AttachedVariantCode]
    , @BatchNo, GetDate() as [Generated at], 3 as [Type] 
  from [ItemAttachment_SET] as [Target] 
  inner join @Source as [Source] on [Source].[Item No_] collate database_default  = [Target].[ItemNo] collate database_default  and [Source].[Variant Code] collate database_default  = [Target].[VariantCode] collate database_default  and [Source].[Attached Item No_] collate database_default  = [Target].[AttachedItemNo] collate database_default  and [Source].[Attached Variant Code] collate database_default  = [Target].[AttachedVariantCode] collate database_default 
  where [Source].[Reciprocally] <> [Target].[Reciprocally] 
end 
 
delete 
from [ItemAttachment_SET] 
 
insert into [ItemAttachment_SET]([ItemNo], [VariantCode], [AttachedItemNo], [AttachedVariantCode], [Reciprocally]) 
select [Item No_], [Variant Code], [Attached Item No_], [Attached Variant Code], [Reciprocally] 
from @Source 
 
update [Export Data Log] 
set [Merge Completed] = 1, 
    [Merge End Date] = getdate() 
where [Batch No_] = @BatchNo 
if (@Preview = 0) 
  commit 
else begin 
  select [Log].* 
  from [Export Data Log] as [Log] 
  inner join [ItemAttachment_LOG] as [Target_Log] on [Log].[Batch No_] = [Target_Log].[Batch No_] 
  where [Log].[Exported] = 0 
     and [Log].[Packages Code] = 'WS_EXPORT' 
     and [Log].[Set Name] = 'ITEMATTACH' 
 
  rollback 
end 
 
